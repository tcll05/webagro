export class Cargos {
	cargoId: number;
	descripcion: string;
	activo: boolean;


	constructor($CargoId: number, $descripcion: string, $activo: boolean) {
		this.cargoId = $CargoId;
		this.descripcion = $descripcion;
		this.activo = $activo;
	}

}

export class Cultivos {
	cultivoId: number;
	descripcion: string;
	costo: number;
	activo: boolean;


	constructor($CultivoId: number, $descripcion: string, $costo: number, $activo: boolean) {
		this.cultivoId = $CultivoId;
		this.descripcion = $descripcion;
		this.costo = $costo;
		this.activo = $activo;
	}

}

export class Actividades {
	actividadId: number;
	descripcion: string;
	costo: number;
	pagoPorHora:number;
	activo: boolean;
	pagoRecurrente: boolean;


	constructor($actividadId: number, $descripcion: string, $costo: number, $activo: boolean, $pagoRecurrente) {
		this.actividadId = $actividadId;
		this.descripcion = $descripcion;
		this.costo = $costo;
		this.activo = $activo;
		this.pagoRecurrente = $pagoRecurrente;
	}

}

export class MetodoPagos {
	metodoPagoId: number;
	descripcion: string;


	constructor($metodoPagoId: number, $descripcion: string) {
		this.metodoPagoId = $metodoPagoId;
		this.descripcion = $descripcion;
	}

}

export class Fincas {
	fincaId: number
	nombre: string;
	descripcion: string;
	direccion: string;
	georeferencia: string;
	activo: boolean;


	constructor($nombre: string, $descripcion: string, $direccion: string, $georeferencia: string, $activo: boolean) {
		this.nombre = $nombre;
		this.descripcion = $descripcion;
		this.direccion = $direccion;
		this.georeferencia = $georeferencia;
		this.activo = $activo;
	}
}

export class Lotes {
	loteId: number;
	fincaId: number;
	codigo: string;
	descripcion: string;
	activo: boolean;
	fechaCreacion: Date;
	finca: Fincas;

	constructor($loteId: number, $fincaId: number, $codigo: string, $descripcion: string, $activo: boolean, $fechaCreacion: Date) {
		this.loteId = $loteId;
		this.fincaId = $fincaId;
		this.codigo = $codigo;
		this.descripcion = $descripcion;
		this.activo = $activo;
		this.fechaCreacion = $fechaCreacion;
	}

}

export class LoteInversiones {
	loteInversionId: number;
	loteNombre:string;
	fincaNombre:string;
	loteCodigo:string;
	loteId: number;
	concepto: string;
	descripcion: string;
	costo: number;
	fechaCreacion: Date;
	lote: Lotes;

	constructor($loteInversionId: number, $loteId: number, $concepto: string, $descripcion: string, $costo: number, $fechaCreacion: Date) {
		this.loteInversionId = $loteInversionId;
		this.loteId = $loteId;
		this.concepto = $concepto;
		this.descripcion = $descripcion;
		this.costo = $costo;
		this.fechaCreacion = $fechaCreacion;
	}

}

export class ActividadEmpleado {
	actividadEmpleadoId: number;
	empleadoId: number;
	fincaId: number;
	actividadId: number;
	actividadCosto: number;
	actividadTotal:number;
	actividadHoras: number;
	empleadoNombre: string;
	actividadNombre: string;
	pagoEfectuado: number;
	pagada: boolean
	fechaCreacion: string;

}

export class ActividadLote {
	loteActividadId: number;
	empleadoId: number;
	fincaId: number;
	loteId: number
	actividadId: number;
	actividadCosto: number;
	empleadoNombre: string;
	actividadNombre: string;
	loteNombre: string;
	pagoEfectuado: number;
	pagada: boolean
	fechaCreacion: string;
	actividadHoras:number;

}

export class Usuarios {
	usuarioId: number
	nombre: string
	contrasenia: string
	activo: boolean
	accesoMovil: boolean	
	accesoWeb:boolean
	fechaCreacion: Date

}

export class Planilla {
	planillaId: number;
	correlativo: number;
	empleadoId: number;
	nombreEmpleado: string;
	metodoPagoId: number;
	tipoOperacion: number;
	fechaInicio: Date;
	fechaFinal: Date;
	pagoEfectuado: number;
	pagoEnLetras: number;
	impreso: boolean;
	fechaImpresion: Date;
	fechaCreacion: Date;

}