import { Empleado } from './Empleados';
import { Cultivos, Lotes } from './Catalogos';

export class Cosechas {
    CosechaId: number;
    EmpleadoId:number;
    CultivoId:number;
    LoteId:number;
    Cantidad:number;
    CultivoPago:number;
    TotalPago:number;
    Pagada:boolean;
    FechaCreacion:Date;
    Empleado: Empleado;
    Cultivo: Cultivos;
    Lote:Lotes;
    
	constructor($CosechaId: number, $EmpleadoId: number, $CultivoId: number, $LoteId: number, $Cantidad: number, $CultivoPago: number, $TotalPago: number, $Pagada: boolean, $FechaCreacion: Date) {
		this.CosechaId = $CosechaId;
		this.EmpleadoId = $EmpleadoId;
		this.CultivoId = $CultivoId;
		this.LoteId = $LoteId;
		this.Cantidad = $Cantidad;
		this.CultivoPago = $CultivoPago;
		this.TotalPago = $TotalPago;
		this.Pagada = $Pagada;
		this.FechaCreacion = $FechaCreacion;
	}

}