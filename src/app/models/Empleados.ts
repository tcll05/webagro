export class Empleado {
    empleadoId: number;
    cargoId: number;
    codigo: string;
    nombres: string;
    apellidos: string;
    documento:string;
    sexo: string;
    fechaNacimiento: Date;
    nacionalidad: string;
    telefono: string;
    correoElectronico: string;
    activo: boolean;
    fechaCreacion: Date;

	constructor($empleadoId: number, $cargoId: number, $codigo: string, $nombres: string, $apellidos: string, $documento: string, $sexo: string, $fechaNacimiento: Date, $nacionalidad: string, $telefono: string, $correoElectronico: string, $activo: boolean, $fechaCreacion: Date) {
		this.empleadoId = $empleadoId;
		this.cargoId = $cargoId;
		this.codigo = $codigo;
		this.nombres = $nombres;
		this.apellidos = $apellidos;
		this.documento = $documento;
		this.sexo = $sexo;
		this.fechaNacimiento = $fechaNacimiento;
		this.nacionalidad = $nacionalidad;
		this.telefono = $telefono;
		this.correoElectronico = $correoElectronico;
		this.activo = $activo;
		this.fechaCreacion = $fechaCreacion;
	}

}