import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './Services/auth.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'WebAgro';
  isLoggedIn$: Observable<boolean>; 
  name = environment.name;
  constructor(private router: Router, private sesion:AuthService) { }

  ngOnInit(): void {
    this.isLoggedIn$ = this.sesion.isLoggedIn;
  }
}
