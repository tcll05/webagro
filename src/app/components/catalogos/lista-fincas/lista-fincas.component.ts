import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrearFincasComponent } from '../crear-fincas/crear-fincas.component';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-lista-fincas',
    templateUrl: './lista-fincas.component.html',
    styleUrls: ['./lista-fincas.component.css']
})
export class ListaFincasComponent implements OnInit {
    saveFinca: Fincas;

    constructor(private alert:AlertService,private modalService: NgbModal, private peticion: PeticionService) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
                `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
            ].join('')
        };
        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.activo) {
                badges = `
                <span class="badge badge-danger">Inactivo</span>
                `;

            }
            if (row.activo) {
                badges = `<span class="badge badge-success">Activo</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            }
        };

        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            url: `${url}/finca`,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            pagination: true,
            search: true,
            showRefresh: true,
            queryParams: queryParams,
            size: 5,
            locale: 'es-CR',
            columns: [
                {
                    field: 'nombre',
                    title: 'Finca'
                }, {
                    field: 'descripcion',
                    title: 'Descripcion'
                },
                {
                    field: 'direccion',
                    title: 'Direccion'
                },
                {
                    field: 'activo',
                    formatter: "estados",
                    title: 'Estado'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 190,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]

        });
    }

    nuevo() {
        const modalRef = this.modalService.open(CrearFincasComponent, { size: 'lg', scrollable: true });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveFinca = receivedEntry;
                //hacemos el post mediante el servicio de peticion
                this.peticion.post<any>("finca/", this.saveFinca).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )
            })
    }

    editar(finca: Fincas) {
        const modalRef = this.modalService.open(CrearFincasComponent, { scrollable: true, size: 'lg' });
        //enviamos la persona a modificar al componente de crear-persona
        modalRef.componentInstance.tmpFinca = finca;

        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveFinca = receivedEntry;
                this.saveFinca.activo=true;
                this.peticion.put<any>("finca", this.saveFinca, receivedEntry.fincaId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue modificado...',
                                'error'
                            )
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al modificar el registro!',
                            'error'
                        )
                    }
                )

                //this.data.push(this.savePersona);

            })
    }

    eliminar(finca: Fincas) {
        finca.activo = false;
        Swal.fire({
            title: 'Eliminar Registro',
            text: `Seguro desea eliminar el registro de ${finca.nombre}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                this.peticion.put<any>("finca", finca, finca.fincaId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro eliminado!',
                                'El registro ha sido eliminado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue eliminado...',
                                'error'
                            );
                                (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al eliminar el registro!',
                            'error'
                        )
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });

    }
    getRow(row) {
        console.log(row)
    }

}
