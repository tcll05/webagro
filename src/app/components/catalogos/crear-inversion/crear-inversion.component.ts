import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoteInversiones, Lotes } from 'src/app/models/Catalogos';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-crear-inversion',
  templateUrl: './crear-inversion.component.html',
  styleUrls: ['./crear-inversion.component.css']
})
export class CrearInversionComponent implements OnInit {

  formInversion: FormGroup;
  inversion: LoteInversiones;

  lotes: Lotes[]=[]
  //Variable de entrada para modificar una Lote registrada
  @Input() public tmpLote: LoteInversiones;
  //variable de salida para retornar la Lote que se registro
  @Output() passEntry: EventEmitter<LoteInversiones> = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private peticion: PeticionService) { }


  ngOnInit(): void {
    this.getLotes();
    this.formInversion = this.formBuilder.group({
      loteId: [0, [Validators.required]],
      concepto: [null, [Validators.required, Validators.maxLength(25)]],
      descripcion: [null, [Validators.required, Validators.maxLength(150)]],
      costo: [0.0, [Validators.required]]
    })
    
    if (!isNullOrUndefined(this.tmpLote))
      this.formInversion.patchValue(this.tmpLote);
  }

  getLotes() {
    this.peticion.getAll<Lotes[]>("lote").subscribe(
      result => {
        result.forEach(lote => {
          if (lote.activo) {
            this.lotes.push(lote)
          }
        });
      }
    )
  }

  guardar() {
    /*si el formulario es invalido (si no cumple con todas las validaciones)
    * muestra los mensajes de validacion         
    */
    if (this.formInversion.invalid) {
      this.formInversion.markAllAsTouched();
      return;
    }
    else {
      // al modelo Lote le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
      this.inversion = this.formInversion.value;
      //si la variable de entrada tmpLote (modificar) no es nula, 
      //le asigna el id al objeto Lote que sera registrado
      if (this.tmpLote != null)
        this.inversion.loteInversionId = this.tmpLote.loteInversionId;
      // retornamos a la pantalla de listado el objeto Lote con todos los datos que fueron ingresados
      this.passEntry.emit(this.inversion);
      //se vacian los campos y se cierra la modal
      this.formInversion.reset();
      this.activeModal.close();
    }

  }

  validarLote() {
    if (this.formInversion.get('loteId').value == 0)
      return true;
    else return false;
  }

}
