import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearInversionComponent } from './crear-inversion.component';

describe('CrearInversionComponent', () => {
  let component: CrearInversionComponent;
  let fixture: ComponentFixture<CrearInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
