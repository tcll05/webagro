import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosRoutingModule } from './catalogos-routing.module';
import { CargosComponent } from './cargos/cargos.component';
import { ActividadesComponent } from './actividades/actividades.component';

import { NgxCurrencyModule, CurrencyMaskInputMode } from "ngx-currency";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearFincasComponent } from './crear-fincas/crear-fincas.component';
import { ListaFincasComponent } from './lista-fincas/lista-fincas.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrearLoteComponent } from './crear-lote/crear-lote.component';
import { ListaLotesComponent } from './lista-lotes/lista-lotes.component';
import { CultivosComponent } from './cultivos/cultivos.component';
import { ListaInversionComponent } from './lista-inversion/lista-inversion.component';
import { CrearInversionComponent } from './crear-inversion/crear-inversion.component';
export const customCurrencyMaskConfig = {
  align: "left",
  allowNegative: false,
  allowZero: true,
  decimal: ".",
  precision: 2,
  prefix: "L. ",
  suffix: "",
  thousands: ",",
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.FINANCIAL
};
@NgModule({
  declarations: [CargosComponent, ActividadesComponent, CrearFincasComponent, ListaFincasComponent, CrearLoteComponent, ListaLotesComponent, CultivosComponent, ListaInversionComponent, CrearInversionComponent],
  imports: [
    CommonModule,
    CatalogosRoutingModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  entryComponents:[
    CrearFincasComponent,
    CrearLoteComponent
  ]
})
export class CatalogosModule { }
