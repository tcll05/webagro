import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Cultivos } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UtilsService } from 'src/app/Services/utils.service';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-cultivos',
    templateUrl: './cultivos.component.html',
    styleUrls: ['./cultivos.component.css']
})
export class CultivosComponent implements OnInit {

    agregar: boolean = false;
    saveCultivo: Cultivos
    editar: Cultivos;
    nuevoRegistro: boolean = true;
    formCultivo: FormGroup;
    constructor(private alert:AlertService,private utils:UtilsService, private formBuilder: FormBuilder, private peticion: PeticionService) { }

    ngOnInit(): void {
        this.nuevoRegistro = true;
        this.formCultivo = this.formBuilder.group({
            descripcion: [null, [Validators.required, Validators.maxLength(160)]],
            costo: [null, [Validators.required]],
        })
    }
    validarCosto(){
        if(this.formCultivo.get("costo").value <=0)
        return true
        else return false
    }

    ngAfterViewInit() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
                `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
            ].join('')
        };

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.activo) {
                badges = `
                <span class="badge badge-danger">Inactivo</span>
                `;

            }
            if (row.activo) {
                badges = `<span class="badge badge-success">Activo</span>`
            }
            return [
                badges
            ].join('')
        };
        (<any>window).numberFormat = function (value, row, index) {
            console.log(value);
            return self.utils.formatMoney(value);
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.getRow(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            }
        };

        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            url: `${url}/cultivo`,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            pagination: true,
            queryParams: queryParams,
            search: true,
            showRefresh: true,
            size: 5,
            locale: 'es-CR',
            columns: [
                {
                    field: 'descripcion',
                    title: 'Cultivo'
                }, {
                    field: 'costo',
                    formatter:"numberFormat",
                    title: 'Costo'
                },
                {
                    field: 'activo',
                    formatter: 'estados',
                    title: 'Estado'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 190,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]

        });
    }

    nuevo() {
        this.agregar = true;
    }

    guardar() {
        if (this.formCultivo.invalid || this.validarCosto()) {
            this.formCultivo.markAllAsTouched();
        }
        else {
            this.saveCultivo = this.formCultivo.value;
            this.saveCultivo.activo = true;
            //hacemos el post mediante el servicio de peticion
            if (this.nuevoRegistro) {
                this.peticion.post<any>("cultivo/", this.saveCultivo).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            );
                            this.agregar = false;
                            this.nuevoRegistro = true;
                            this.formCultivo.reset();
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )
            }
            else {
                this.peticion.put<any>("cultivo", this.saveCultivo, this.editar.cultivoId).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            this.agregar = false;
                            this.nuevoRegistro = true;
                            this.formCultivo.reset();
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al modificar el registro',
                            'error'
                        )
                    }
                )
            }


        }
    }

    cancelar() {
        this.formCultivo.reset();
        this.agregar = false;
    }

    eliminar(cultivo: Cultivos) {
        cultivo.activo = false;
        Swal.fire({
            title: 'Eliminar Registro',
            text: `Seguro desea eliminar el registro de ${cultivo.descripcion}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                this.peticion.put<any>("cultivo", cultivo, cultivo.cultivoId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro eliminado!',
                                'El registro ha sido eliminado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue eliminado...',
                                'error'
                            );
                                (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al eliminar el registro!',
                            'error'
                        )
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });

    }

    getRow(row) {
        this.nuevoRegistro = false;
        this.agregar = true;
        this.editar = row;
        this.formCultivo.patchValue(row);
    }

}
