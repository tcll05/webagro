import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Lotes, Fincas } from 'src/app/models/Catalogos';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-crear-lote',
  templateUrl: './crear-lote.component.html',
  styleUrls: ['./crear-lote.component.css']
})
export class CrearLoteComponent implements OnInit {

  formLote: FormGroup;
  lote: Lotes;

  fincas: Fincas[]=[]
  //Variable de entrada para modificar una Lote registrada
  @Input() public tmpLote: Lotes;
  //variable de salida para retornar la Lote que se registro
  @Output() passEntry: EventEmitter<Lotes> = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private peticion: PeticionService) { }


  ngOnInit(): void {
    this.getFincas();
    this.formLote = this.formBuilder.group({
      fincaId: [0, [Validators.required]],
      codigo: [null, [Validators.required, Validators.maxLength(25)]],
      descripcion: [null, [Validators.required, Validators.maxLength(150)]],
    })
    
    if (!isNullOrUndefined(this.tmpLote))
      this.formLote.patchValue(this.tmpLote);
  }

  getFincas() {
    this.peticion.getAll<Fincas[]>("finca").subscribe(
      result => {
        result.forEach(finca => {
          if (finca.activo) {
            this.fincas.push(finca)
          }
        });
      }
    )
  }

  guardar() {
    /*si el formulario es invalido (si no cumple con todas las validaciones)
    * muestra los mensajes de validacion         
    */
    if (this.formLote.invalid) {
      this.formLote.markAllAsTouched();
      return;
    }
    else {
      // al modelo Lote le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
      this.lote = this.formLote.value;
      //si la variable de entrada tmpLote (modificar) no es nula, 
      //le asigna el id al objeto Lote que sera registrado
      if (this.tmpLote != null)
        this.lote.loteId = this.tmpLote.loteId;
      // retornamos a la pantalla de listado el objeto Lote con todos los datos que fueron ingresados
      this.passEntry.emit(this.lote);
      //se vacian los campos y se cierra la modal
      this.formLote.reset();
      this.activeModal.close();
    }

  }

  validarFinca() {
    if (this.formLote.get('fincaId').value == 0)
      return true;
    else return false;
  }

}
