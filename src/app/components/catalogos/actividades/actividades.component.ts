import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Actividades } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { PeticionService } from 'src/app/Services/peticion.service';
import { CurrencyPipe } from '@angular/common';
import { UtilsService } from 'src/app/Services/utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-actividades',
    templateUrl: './actividades.component.html',
    styleUrls: ['./actividades.component.css']
})
export class ActividadesComponent implements OnInit {
    agregar: boolean = false;
    saveActividad: Actividades
    editar: Actividades;
    nuevoRegistro: boolean = true;
    formActividad: FormGroup;
    @ViewChild('content')
    content: TemplateRef<any>;
    constructor(private alert: AlertService, private modalService: NgbModal, private utils: UtilsService, private formBuilder: FormBuilder, private peticion: PeticionService) { }

    ngOnInit(): void {

        this.formActividad = this.formBuilder.group({
            descripcion: [null, [Validators.required, Validators.maxLength(160)]],
            costo: [null, [Validators.required]],
            pagoPorHora: [null, [Validators.required]],
            pagoRecurrente: [false, [Validators.required]]
        })
    }

    ngAfterViewInit() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
                `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
            ].join('')
        };

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.activo) {
                badges = `
                <span class="badge badge-danger">Inactivo</span>
                `;

            }
            if (row.activo) {
                badges = `<span class="badge badge-success">Activo</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).numberFormat = function (value, row, index) {
            console.log(value);
            return self.utils.formatMoney(value);
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.getRow(this.content, row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            }
        };

        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            url: `${url}/actividad`,
            pagination: true,
            queryParams: queryParams,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            search: true,
            showRefresh: true,
            size: 5,
            locale: 'es-CR',
            columns: [
                {
                    field: 'descripcion',
                    title: 'Actividad'
                }, {
                    field: 'costo',
                    formatter: "numberFormat",
                    title: 'Costo'
                },
                {
                    field: 'activo',
                    formatter: "estados",
                    title: 'Estado'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 190,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]

        });
    }

    nuevo(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

        }, (reason) => {
        });
    }

    validarCosto() {
        if (this.formActividad.get("costo").value <= 0 || this.formActividad.get("pagoPorHora").value <= 0)
            return true
        else return false
    }

    guardar() {
        if (this.formActividad.invalid || this.validarCosto()) {
            this.formActividad.markAllAsTouched();
        }
        else {
            this.saveActividad = this.formActividad.value;
            this.saveActividad.activo = true;
            //hacemos el post mediante el servicio de peticion
            if (this.nuevoRegistro) {
                this.peticion.post<any>("actividad/", this.saveActividad).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            );
                            this.agregar = false;
                            this.nuevoRegistro = true;
                            this.formActividad.reset();
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )
            }
            else {
                this.peticion.put<any>("actividad", this.saveActividad, this.editar.actividadId).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            this.agregar = false;
                            this.nuevoRegistro = true;
                            this.formActividad.reset();
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al modificar el registro',
                            'error'
                        )
                    }
                )
            }


        }
    }

    cancelar() {
        this.formActividad.reset();
        this.agregar = false;
    }

    eliminar(actividad: Actividades) {
        actividad.activo = false;
        Swal.fire({
            title: 'Eliminar Registro',
            text: `Seguro desea eliminar el registro de ${actividad.descripcion}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                this.peticion.put<any>("actividad", actividad, actividad.actividadId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro eliminado!',
                                'El registro ha sido eliminado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue eliminado...',
                                'error'
                            );
                            (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al eliminar el registro!',
                            'error'
                        )
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });

    }

    getRow(content, row) {
        this.nuevoRegistro = false;
        this.agregar = true;

        this.editar = row;
        this.formActividad.patchValue(row);
        this.nuevo(this.content);
    }

}
