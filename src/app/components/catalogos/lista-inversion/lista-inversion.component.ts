import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { LoteInversiones } from 'src/app/models/Catalogos';
import { CrearInversionComponent } from '../crear-inversion/crear-inversion.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UtilsService } from 'src/app/Services/utils.service';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
  selector: 'app-lista-inversion',
  templateUrl: './lista-inversion.component.html',
  styleUrls: ['./lista-inversion.component.css']
})
export class ListaInversionComponent implements OnInit {

  saveInversion: LoteInversiones;

  constructor(private alert:AlertService, private modalService: NgbModal, private utils:UtilsService, private peticion: PeticionService, private util: UtilsService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    var self = this;
    (<any>window).acciones = function (value, row, index) {
      return [
        `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
        `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
      ].join('')
    };

    (<any>window).estados = function (value, row, index) {
      let badges = "";
      if (!row.activo) {
        badges = `
          <span class="badge badge-danger">Inactivo</span>
          `;

      }
      if (row.activo) {
        badges = `<span class="badge badge-success">Activo</span>`
      }
      return [
        badges
      ].join('')
    };

    (<any>window).operateEvents = {
      'click .edit': function (e, value, row, index) {
        self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
      },
      'click .delete': function (e, value, row, index) {
        self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
      }
    };

    (<any>window).numberFormat = function (value, row, index) {
      console.log(value);
      return self.utils.formatMoney(value);
    };

    function queryParams() {
      return {
      };
    }
    function dateFormatter(value, row, index) {
      return self.util.dateTimeFormatter(value);
    };

    let url = this.peticion.getBaseURL();
    (<any>$('#table')).bootstrapTable({
      url: `${url}/lote/inversiones`,
      ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
      pagination: true,
      search: true,
      queryParams: queryParams,
      size: 100,
      exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
      locale: 'es-CR',
      showExport: true,
      columns: [
        {
          field: 'fincaNombre',
          title: 'Finca'
        },
        {
          field: 'loteCodigo',
          title: 'Codigo de lote'
        },
        {
          field: 'loteNombre',
          title: 'Lote'
        }, {
          field: 'concepto',
          title: 'Concepto'
        },
        {
          field: 'descripcion',
          title: 'Descripcion'
        },
        {
          field: 'costo',
          formatter:"numberFormat",
          title: 'Costo'
        },
        {
          field: 'fechaCreacion',
          formatter: dateFormatter,
          title: 'Fecha'
        },
        {
          field: 'acciones',
          title: 'Acciones',
          width: 190,
          widthUnit: "px",
          formatter: "acciones",
          events: "operateEvents"
        }
      ]

    });
  }

  nuevo() {
    const modalRef = this.modalService.open(CrearInversionComponent, { size: 'lg' });
    //nos suscribimos para recibir los datos registrados en el formulario
    modalRef.componentInstance.passEntry.subscribe(
      (receivedEntry) => {
        this.saveInversion = receivedEntry;
        this.saveInversion.loteId = +this.saveInversion.loteId;
        this.peticion.post<any>("lote/inversiones", this.saveInversion).subscribe(
          result => {
            if (result.resultado == true) { //si el resultado es correcto
              this.alert.mensaje(
                'Registro agregado!',
                'El registro ha sido creado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error!',
                result.mensaje,
                'error'
              )
            }

          },
          error => {
            this.alert.mensaje(
              'Error!',
              'Ha ocurrido un error al crear el registro',
              'error'
            )
          }
        )
      })
  }

  editar(lote: LoteInversiones) {
    const modalRef = this.modalService.open(CrearInversionComponent, { scrollable: true, size: 'lg' });
    //enviamos la persona a modificar al componente de crear-persona
    modalRef.componentInstance.tmpLote = lote;

    modalRef.componentInstance.passEntry.subscribe(
      (receivedEntry) => {
        this.saveInversion = receivedEntry;
        this.saveInversion.loteId = +this.saveInversion.loteId;
        this.peticion.put<any>("lote/inversiones", this.saveInversion, receivedEntry.loteInversionId).subscribe(
          result => {
            if (result.resultado == true) {
              this.alert.mensaje(
                'Registro modificado!',
                'El registro ha sido modificado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error',
                'El registro no fue modificado...',
                'error'
              );
                (<any>$('#table')).bootstrapTable('refresh')
            }
          },
          error => {
            this.alert.mensaje(
              'Error',
              'Error al modificar el registro!',
              'error'
            )
          }
        )
      })
  }

  eliminar(lote: LoteInversiones) {
    Swal.fire({
      title: 'Eliminar Registro',
      text: `Seguro desea eliminar el registro de ${lote.concepto}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.peticion.delete<any>("lote/inversiones", lote.loteInversionId).subscribe(
          result => {
            if (result.resultado == true) {
              this.alert.mensaje(
                'Registro eliminado!',
                'El registro ha sido eliminado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error',
                'El registro no fue eliminado...',
                'error'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
          },
          error => {
            this.alert.mensaje(
              'Error',
              'Error al eliminar el registro!',
              'error'
            )
          }
        )

      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });

  }

}
