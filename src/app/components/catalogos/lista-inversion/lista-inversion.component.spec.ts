import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaInversionComponent } from './lista-inversion.component';

describe('ListaInversionComponent', () => {
  let component: ListaInversionComponent;
  let fixture: ComponentFixture<ListaInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
