import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearFincasComponent } from './crear-fincas.component';

describe('CrearFincasComponent', () => {
  let component: CrearFincasComponent;
  let fixture: ComponentFixture<CrearFincasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearFincasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearFincasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
