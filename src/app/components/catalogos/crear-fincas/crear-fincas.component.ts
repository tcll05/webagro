import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Fincas } from 'src/app/models/Catalogos';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { isNullOrUndefined } from 'util';
import { PeticionService } from 'src/app/Services/peticion.service';

@Component({
  selector: 'app-crear-fincas',
  templateUrl: './crear-fincas.component.html',
  styleUrls: ['./crear-fincas.component.css']
})
export class CrearFincasComponent implements OnInit {
  formFinca: FormGroup;
  finca: Fincas;
  //Variable de entrada para modificar una Finca registrada
  @Input() public tmpFinca: Fincas;
  //variable de salida para retornar la Finca que se registro
  @Output() passEntry: EventEmitter<Fincas> = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal) { }


  ngOnInit(): void {
    this.formFinca = this.formBuilder.group({
      nombre: [null, [Validators.required, Validators.maxLength(120)]],
      descripcion: [null, [Validators.required, Validators.maxLength(150)]],
      direccion: [null, [Validators.required]],
      //georeferencia: [null, [Validators.required, Validators.maxLength(120)]],
    })
    if (!isNullOrUndefined(this.tmpFinca))
      this.formFinca.patchValue(this.tmpFinca);
  }

  

  guardar() {
    /*si el formulario es invalido (si no cumple con todas las validaciones)
    * muestra los mensajes de validacion         
    */
    if (this.formFinca.invalid) {
      this.formFinca.markAllAsTouched();
      return;
    }
    else {
      // al modelo Finca le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
      this.finca = this.formFinca.value;
      //si la variable de entrada tmpFinca (modificar) no es nula, 
      //le asigna el id al objeto Finca que sera registrado
      if (!isNullOrUndefined(this.tmpFinca))
        this.finca.fincaId = this.tmpFinca.fincaId;
      // retornamos a la pantalla de listado el objeto Finca con todos los datos que fueron ingresados
      this.passEntry.emit(this.finca);
      //se vacian los campos y se cierra la modal
      this.formFinca.reset();
      this.activeModal.close();
    }

  }
}
