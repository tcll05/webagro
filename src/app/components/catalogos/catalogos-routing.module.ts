import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CargosComponent } from './cargos/cargos.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { ListaFincasComponent } from './lista-fincas/lista-fincas.component';
import { ListaLotesComponent } from './lista-lotes/lista-lotes.component';
import { CultivosComponent } from './cultivos/cultivos.component';
import { ListaInversionComponent } from './lista-inversion/lista-inversion.component';


const routes: Routes = [
  {
    path: 'cargos',
    component: CargosComponent
  },
  {
    path: 'actividades',
    component: ActividadesComponent
  },
  {
    path: 'fincas',
    component: ListaFincasComponent
  },
  {
    path: 'lotes',
    component: ListaLotesComponent
  },
  {
    path: 'cultivos',
    component: CultivosComponent
  },
  {
    path: 'inversiones',
    component: ListaInversionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogosRoutingModule { }
