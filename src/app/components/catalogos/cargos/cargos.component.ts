import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cargos } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertService } from 'src/app/Services/alert.service';
@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit {
  agregar: boolean = false;
  formCargos: FormGroup;
  saveCargos: Cargos;
  nuevoRegistro: boolean;
  editar: Cargos;
  constructor(private alert:AlertService,private peticion: PeticionService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.nuevoRegistro = true
    this.formCargos = this.formBuilder.group({
      descripcion: [null, [Validators.required, Validators.maxLength(255)]]
    })
  }

  ngAfterViewInit() {
    var self = this;
    (<any>window).acciones = function (value, row, index) {
      return [
        `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
        `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
      ].join('')
    };

    (<any>window).estados = function (value, row, index) {
      let badges = "";
      if (!row.activo) {
        badges = `
          <span class="badge badge-danger">Inactivo</span>
          `;

      }
      if (row.activo) {
        badges = `<span class="badge badge-success">Activo</span>`
      }
      return [
        badges
      ].join('')
    };

    (<any>window).operateEvents = {
      'click .edit': function (e, value, row, index) {
        self.getRow(row);//alert('You click like action, row: ' + JSON.stringify(row))
      },
      'click .delete': function (e, value, row, index) {
        self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
      }
    };
    function queryParams() {
      return {
      };
    }

    let url = this.peticion.getBaseURL();
    (<any>$('#table')).bootstrapTable({
      url: `${url}/cargo`,
      ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
      pagination: true,
      search: true,
      showRefresh: true,
      queryParams: queryParams,
      size: 5,
      locale: 'es-CR',
      columns: [
        {
          field: 'descripcion',
          title: 'Cargo'
        }, {
          field: 'activo',
          formatter: "estados",
          title: 'Estado'
        },
        {
          field: 'acciones',
          title: 'Acciones',
          width: 190,
          widthUnit: "px",
          formatter: "acciones",
          events: "operateEvents"
        }
      ]

    });
  }

  nuevo() {
    this.agregar = true;
  }

  guardar() {
    if (this.formCargos.invalid) {
      this.formCargos.markAllAsTouched();
      return;
    }
    else {
      this.saveCargos = this.formCargos.value;
      this.saveCargos.activo = true;
      if (this.nuevoRegistro) {
        this.peticion.post<any>("cargo", this.saveCargos).subscribe(
          result => {
            if (result.resultado) {
              this.agregar = false;
              this.formCargos.reset();
              this.nuevoRegistro = true;
              (<any>$('#table')).bootstrapTable('refresh');
              this.alert.mensaje(
                'Registro agregado!',
                'El registro ha sido creado con exito',
                'success'
              )
            }

          }
        )
      }
      else {
        this.saveCargos.cargoId = this.editar.cargoId;

        this.peticion.put<any>("cargo", this.saveCargos, this.editar.cargoId).subscribe(
          result => {
            if (result.resultado) {
              this.agregar = false;
              this.formCargos.reset();
              this.nuevoRegistro = true;
              (<any>$('#table')).bootstrapTable('refresh');
              this.alert.mensaje(
                'Registro modificado!',
                'El registro ha sido modificaco con exito',
                'success'
              )
            }

          }
        )
      }

    }
  }

  cancelar() {
    this.agregar = false;
    this.formCargos.reset();
    this.nuevoRegistro = true;
  }

  eliminar(cargo: Cargos) {
    cargo.activo = false;
    Swal.fire({
      title: 'Eliminar Registro',
      text: `Seguro desea eliminar el registro de ${cargo.descripcion}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.peticion.put<any>("cargo", cargo, cargo.cargoId).subscribe(
          result => {
            if (result.resultado == true) {
              this.alert.mensaje(
                'Registro eliminado!',
                'El registro ha sido eliminado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error',
                'El registro no fue eliminado...',
                'error'
              );
              (<any>$('#table')).bootstrapTable('refresh')
            }
          },
          error => {
            this.alert.mensaje(
              'Error',
              'Error al eliminar el registro!',
              'error'
            );
          }
        )

      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });

  }

  getRow(row) {
    this.nuevoRegistro = false;
    this.agregar = true;
    this.editar = row;
    this.formCargos.patchValue(row);
  }

}
