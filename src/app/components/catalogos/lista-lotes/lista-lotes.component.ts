import { Component, OnInit } from '@angular/core';
import { CrearLoteComponent } from '../crear-lote/crear-lote.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Lotes } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
  selector: 'app-lista-lotes',
  templateUrl: './lista-lotes.component.html',
  styleUrls: ['./lista-lotes.component.css']
})
export class ListaLotesComponent implements OnInit {

  saveLote: Lotes;

  constructor(private alert:AlertService, private modalService: NgbModal, private peticion: PeticionService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    var self = this;
    (<any>window).acciones = function (value, row, index) {
      return [
        `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
        `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
      ].join('')
    };

    (<any>window).estados = function (value, row, index) {
      let badges = "";
      if (!row.activo) {
        badges = `
          <span class="badge badge-danger">Inactivo</span>
          `;

      }
      if (row.activo) {
        badges = `<span class="badge badge-success">Activo</span>`
      }
      return [
        badges
      ].join('')
    };

    (<any>window).operateEvents = {
      'click .edit': function (e, value, row, index) {
        self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
      },
      'click .delete': function (e, value, row, index) {
        self.eliminar(row);//alert('You click like action, row: ' + JSON.stringify(row))
      }
    };

    function queryParams() {
      return {
      };
    }

    let url = this.peticion.getBaseURL();
    (<any>$('#table')).bootstrapTable({
      url: `${url}/lote`,
      ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
      pagination: true,
      search: true,
      queryParams: queryParams,
      size: 5,
      showRefresh: true,
      locale: 'es-CR',
      columns: [
        {
          field: 'fincaNombre',
          title: 'Finca'
        }, {
          field: 'codigo',
          title: 'Codigo'
        },
        {
          field: 'descripcion',
          title: 'Descripcion'
        },
        {
          field: 'activo',
          formatter: "estados",
          title: 'Estado'
        },
        {
          field: 'acciones',
          title: 'Acciones',
          width: 190,
          widthUnit: "px",
          formatter: "acciones",
          events: "operateEvents"
        }
      ]

    });
  }

  nuevo() {
    const modalRef = this.modalService.open(CrearLoteComponent, { size: 'lg' });
    //nos suscribimos para recibir los datos registrados en el formulario
    modalRef.componentInstance.passEntry.subscribe(
      (receivedEntry) => {
        this.saveLote = receivedEntry;
        this.saveLote.fincaId = +this.saveLote.fincaId;
        this.saveLote.activo = true;
        this.peticion.post<any>("lote/", this.saveLote).subscribe(
          result => {
            if (result.resultado == true) { //si el resultado es correcto
              this.alert.mensaje(
                'Registro agregado!',
                'El registro ha sido creado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error!',
                result.mensaje,
                'error'
              )
            }

          },
          error => {
            this.alert.mensaje(
              'Error!',
              'Ha ocurrido un error al crear el registro',
              'error'
            )
          }
        )
      })
  }

  editar(lote: Lotes) {
    const modalRef = this.modalService.open(CrearLoteComponent, { scrollable: true, size: 'lg' });
    //enviamos la persona a modificar al componente de crear-persona
    modalRef.componentInstance.tmpLote = lote;

    modalRef.componentInstance.passEntry.subscribe(
      (receivedEntry) => {
        this.saveLote = receivedEntry;
        this.saveLote.fincaId = +this.saveLote.fincaId;
        this.saveLote.activo = true;
        this.peticion.put<any>("lote", this.saveLote, receivedEntry.loteId).subscribe(
          result => {
            if (result.resultado == true) {
              this.alert.mensaje(
                'Registro modificado!',
                'El registro ha sido modificado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error',
                'El registro no fue modificado...',
                'error'
              );
                (<any>$('#table')).bootstrapTable('refresh')
            }
          },
          error => {
            this.alert.mensaje(
              'Error',
              'Error al modificar el registro!',
              'error'
            )
          }
        )
      })
  }

  eliminar(lote: Lotes) {
    lote.activo = false;
    Swal.fire({
      title: 'Eliminar Registro',
      text: `Seguro desea eliminar el registro de ${lote.descripcion}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.peticion.put<any>("lote", lote, lote.loteId).subscribe(
          result => {
            if (result.resultado == true) {
              this.alert.mensaje(
                'Registro eliminado!',
                'El registro ha sido eliminado con exito',
                'success'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
            else {
              this.alert.mensaje(
                'Error',
                'El registro no fue eliminado...',
                'error'
              );
              (<any>$('#table')).bootstrapTable('refresh');
            }
          },
          error => {
            this.alert.mensaje(
              'Error',
              'Error al eliminar el registro!',
              'error'
            )
          }
        )

      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });

  }

}
