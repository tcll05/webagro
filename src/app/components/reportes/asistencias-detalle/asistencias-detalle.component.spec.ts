import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciasDetalleComponent } from './asistencias-detalle.component';

describe('AsistenciasDetalleComponent', () => {
  let component: AsistenciasDetalleComponent;
  let fixture: ComponentFixture<AsistenciasDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsistenciasDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenciasDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
