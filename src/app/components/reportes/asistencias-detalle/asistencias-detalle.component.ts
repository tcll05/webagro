import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from 'src/app/Services/utils.service';

@Component({
  selector: 'app-asistencias-detalle',
  templateUrl: './asistencias-detalle.component.html',
  styleUrls: ['./asistencias-detalle.component.css']
})
export class AsistenciasDetalleComponent implements OnInit {

  @Input() public asistenciasDetalle: { empleado: string, data:any };
  $table2: any;

  constructor(public activeModal: NgbActiveModal, private utils: UtilsService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    this.iniciarTabla2();
  }

  iniciarTabla2() {
    var self = this;

    function datetimeFormatter(value, row, index) {
      return self.utils.dateFormatter(value)
    };

    (<any>$('#table1')).bootstrapTable({
      data: self.asistenciasDetalle.data,
      pagination: true,
      sortName: 'fecha',
      sortOrder: 'desc',
      search: true,
      size: 100,
      exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
      locale: 'es-CR',
      showExport: true,
      columns: [
        {
          field: 'fecha',
          sortable: true,
          formatter: datetimeFormatter,
          title: 'Fecha'
        },
        {
          field: 'asistencia',
          title: 'Asistencia',
          sortable: true,
        }
      ]
    });
    self.$table2 = $('#table1');
  }


}
