import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { ReporteActividadesComponent } from './reporte-actividades/reporte-actividades.component';
import { ReporteCultivosComponent } from './reporte-cultivos/reporte-cultivos.component';
import { ReporteInversionesComponent } from './reporte-inversiones/reporte-inversiones.component';
import { ReportePlanillaComponent } from './reporte-planilla/reporte-planilla.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DetallePlanillaCosechaComponent } from './detalle-planilla-cosecha/detalle-planilla-cosecha.component';
import { DetallePlanillaActividadLoteComponent } from './detalle-planilla-actividad-lote/detalle-planilla-actividad-lote.component';
import { DetallePlanillaActividadFincaComponent } from './detalle-planilla-actividad-finca/detalle-planilla-actividad-finca.component';
import { TransaccionesModule } from '../transacciones/transacciones.module';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { AsistenciasDetalleComponent } from './asistencias-detalle/asistencias-detalle.component';


@NgModule({
  declarations: [ReporteActividadesComponent, ReporteCultivosComponent, ReporteInversionesComponent, ReportePlanillaComponent, DetallePlanillaCosechaComponent, DetallePlanillaActividadLoteComponent, DetallePlanillaActividadFincaComponent, AsistenciasComponent, AsistenciasDetalleComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TransaccionesModule
  ],
  exports:[
    ReporteActividadesComponent
  ]
})
export class ReportesModule { }
