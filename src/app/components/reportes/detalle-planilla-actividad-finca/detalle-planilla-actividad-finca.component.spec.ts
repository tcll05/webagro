import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePlanillaActividadFincaComponent } from './detalle-planilla-actividad-finca.component';

describe('DetallePlanillaActividadFincaComponent', () => {
  let component: DetallePlanillaActividadFincaComponent;
  let fixture: ComponentFixture<DetallePlanillaActividadFincaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePlanillaActividadFincaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePlanillaActividadFincaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
