import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas, Lotes, Cultivos, Planilla } from 'src/app/models/Catalogos';
import { Empleado } from 'src/app/models/Empleados';
import moment from 'moment';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { UtilsService } from 'src/app/Services/utils.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Services/alert.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AsistenciasDetalleComponent } from '../asistencias-detalle/asistencias-detalle.component';

@Component({
  selector: 'app-asistencias',
  templateUrl: './asistencias.component.html',
  styleUrls: ['./asistencias.component.css']
})
export class AsistenciasComponent implements OnInit {

  tipoOperacionSelected: string = '0';
  empleadosSelected: number = 0;
  fechaInicio: string;
  fechaFinal: string;
  @ViewChild('content')
  content: TemplateRef<any>;
  empleado: string = "";
  asistencias: any[];
  $table: any;
  constructor(private alert: AlertService, private modalService: NgbModal, private peticion: PeticionService, private router: Router, private utils: UtilsService) { }

  ngOnInit(): void {
    //this.getData();
  }

  getData() {
    if (this.tipoOperacionSelected == '0') {
      this.alert.mensaje(
        'Error',
        "Por favor, seleccione un tipo de operacion",
        'warning'
      )
    }
    else {
      this.showLoading("Cargando datos...")
      this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
      this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
      let params = {
        fechaInicio: this.fechaInicio,
        fechaFinal: this.fechaFinal
      }
      this.peticion.getAll<any>(`reportes/asistencias/${params.fechaInicio}/${params.fechaFinal}/${this.tipoOperacionSelected}`).subscribe(
        result => {
          Swal.close();
          if (result.resultado == true) {
            this.asistencias = result.informacion;
            this.$table.bootstrapTable('load', this.asistencias)
          }
          else {
            this.alert.mensaje("Error", result.mensaje, "error");
          }
        },
        error => {
          Swal.close();
          this.alert.mensaje("Error", error.error.mensaje, "error");
        }
      )
    }

  }

  showLoading(title) {
    Swal.fire({
      title: title,
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  ngAfterViewInit() {
    var self = this;
    /*(<any>$('.select2bs4')).select2({
      theme: 'bootstrap4'
    });*/
    (<any>$('#reservation')).daterangepicker(function (start, end, label) {
      self.fechaInicio = start;
      self.fechaFinal = end;
    });

    (<any>window).acciones = function (value, row, index) {
      return [
        `<button class="btn btn-block btn-primary btn-sm edit mr-2">Ver detalles</button>`
      ].join('')
    };

    (<any>window).operateEvents = {
      'click .edit': function (e, value, row, index) {
        self.empleado = `${row.nombres} ${row.apellidos}`;
        self.verDetalle(row.empleadoId);//alert('You click like action, row: ' + JSON.stringify(row))
      }
    };

    function nombreFormatter(value, row, index) {
      return `${row.nombres} ${row.apellidos}`;
    };

    function priceFormatter(value, row, index) {
      return self.utils.formatMoney(value);
    };

    self.$table = $('#table');


    (<any>$('#table')).bootstrapTable({
      data: self.asistencias,
      pagination: true,
      sortName: 'codigo',
      sortOrder: 'desc',
      search: true,
      size: 100,
      exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
      locale: 'es-CR',
      showExport: true,
      columns: [
        {
          field: 'codigo',
          sortable: true,
          title: 'Codigo'
        },
        {
          field: 'nombres',
          title: 'Empleado',
          formatter: nombreFormatter,
          sortable: true,
        },
        {
          field: 'acciones',
          title: 'Acciones',
          width: 120,
          widthUnit: "px",
          formatter: "acciones",
          events: "operateEvents"
        }
      ]
    });


  }

  verDetalle(id) {
    if (this.tipoOperacionSelected == '0') {
      this.alert.mensaje(
        'Error',
        "Por favor, seleccione un tipo de operacion",
        'warning'
      )
    }
    else {
      this.showLoading("Cargando datos...")
      this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
      this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
      let params = {
        fechaInicio: this.fechaInicio,
        fechaFinal: this.fechaFinal
      }
      this.peticion.getAll<any>(`reportes/asistencias/detalle/${params.fechaInicio}/${params.fechaFinal}/${id}`).subscribe(
        result => {
          Swal.close();
          if (result.resultado == true) {
            let detalle: [];
            if (this.tipoOperacionSelected === "F") {
              detalle = result.actividadesFinca;
            }
            else{
              detalle = result.actividadesLote;
            }
            //let detalle = result.informacion;
            const modalRef = this.modalService.open(AsistenciasDetalleComponent, { scrollable: true, centered: true, size: "lg" });
            modalRef.componentInstance.asistenciasDetalle = {
              empleado: this.empleado,
              data: detalle
            };

          }
          else {
            this.alert.mensaje("Error", result.mensaje, "error");
          }
        },
        error => {
          Swal.close();
          this.alert.mensaje("Error", error.error.mensaje, "error");
        }
      )
    }

  }

}
