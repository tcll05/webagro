import { Component, OnInit, Input } from '@angular/core';
import * as jquery from 'jquery';
import 'select2';
import { PeticionService } from 'src/app/Services/peticion.service';
import { ActividadLote, Fincas, Lotes } from 'src/app/models/Catalogos';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/Services/utils.service';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { isNullOrUndefined } from 'util';
import moment from 'moment';

@Component({
    selector: 'app-reporte-actividades',
    templateUrl: './reporte-actividades.component.html',
    styleUrls: ['./reporte-actividades.component.css']
})
export class ReporteActividadesComponent implements OnInit {
    fincaSelected: number = 0;
    lotesSelected: number = 0;
    agrupar: boolean = false;
    pagada: boolean = false;
    fincas: Fincas[] = [];
    lotes: Lotes[] = [];

    tabla: string = "";

    fechaInicio: string;
    fechaFinal: string;
    constructor(private peticion: PeticionService, private router: Router, private utils: UtilsService) { }

    ngOnInit(): void {
        this.getFincas();
    }

    getFincas() {
        this.peticion.getAll<Fincas[]>("finca").subscribe(
            result => {
                this.fincas = result;
            },
            error => {

            }
        )
    }

    limpiar() {
        this.tabla = "";
        this.fincaSelected = 0
        this.lotesSelected = 0
        this.agrupar = false
        this.pagada = false
    }

    planillaActividad(tipo: number) {
        this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
        let params = {
            fincaId: +this.fincaSelected,
            loteId: +this.lotesSelected,
            fechaInicio: this.fechaInicio,
            fechaFinal: this.fechaFinal,
            agrupar: this.agrupar,
            pagada: this.pagada,
            reporte: true
        }

        sessionStorage.setItem("params", JSON.stringify(params));

        if (tipo == 1) {
            this.tabla = "empleado";
        }
        else {
            this.tabla = "lote";
        }

    }

    getData() {

    }

    getLotes(finca: Number) {
        //let fincaId = this.fincaSelected.fincaId 
        console.log(finca)
        if (!isNullOrUndefined(finca)) {
            this.peticion.getAll<Lotes[]>(`lote/finca/${finca}`).subscribe(
                result => {
                    this.lotes = result;
                },
                error => {

                }
            )
        }

    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#reservation')).daterangepicker();
        function queryParams() {
            return {
            };
        }
    }

}
