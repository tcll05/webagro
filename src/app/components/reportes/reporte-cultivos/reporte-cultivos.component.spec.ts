import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteCultivosComponent } from './reporte-cultivos.component';

describe('ReporteCultivosComponent', () => {
  let component: ReporteCultivosComponent;
  let fixture: ComponentFixture<ReporteCultivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteCultivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteCultivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
