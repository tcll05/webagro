import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { UtilsService } from 'src/app/Services/utils.service';
import { Fincas, Lotes, Cultivos } from 'src/app/models/Catalogos';
import { Empleado } from 'src/app/models/Empleados';
import { isNullOrUndefined } from 'util';
import { Cosechas } from 'src/app/models/Cosechas';
import Swal from "sweetalert2/dist/sweetalert2.js";
import moment from 'moment';
@Component({
    selector: 'app-reporte-cultivos',
    templateUrl: './reporte-cultivos.component.html',
    styleUrls: ['./reporte-cultivos.component.css']
})
export class ReporteCultivosComponent implements OnInit {
    fincaSelected: number = 0;
    lotesSelected: number = 0;
    empleadosSelected: number = 0;
    cultivosSelected: number = 0;
    agrupar: boolean = false;
    pagada: boolean = false;
    /////////////////////////////////
    fincas: Fincas[] = [];
    lotes: Lotes[] = [];
    cultivos: Cultivos[] = [];
    empleados: Empleado[] = [];
    cosechas: Cosechas[] = []
    fechaInicio: string;
    fechaFinal: string;
    $table: any;
    constructor(private peticion: PeticionService, private utils: UtilsService) { }

    ngOnInit(): void {
        this.getFincas();
        this.getEmpleados();
        this.getCultivos();
        this.getCosechas(true);
    }

    getFincas() {
        this.peticion.getAll<Fincas[]>("finca").subscribe(
            result => {
                this.fincas = result;
            },
            error => {

            }
        )
    }

    getLotes(finca: Number) {
        //let fincaId = this.fincaSelected.fincaId 
        console.log(finca)
        if (!isNullOrUndefined(finca)) {
            this.peticion.getAll<Lotes[]>(`lote/finca/${finca}`).subscribe(
                result => {
                    this.lotes = result;
                },
                error => {

                }
            )
        }

    }

    getCultivos() {
        this.peticion.getAll<Cultivos[]>(`cultivo/`).subscribe(
            result => {
                this.cultivos = result;
            },
            error => {

            }
        )
    }

    getEmpleados() {
        this.peticion.getAll<Empleado[]>(`empleado`).subscribe(
            result => {
                this.empleados = result;
            },
            error => {

            }
        )
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

    getCosechas(initial: boolean) {
        this.showLoading("Cargando datos...");
        if (initial) {
            this.fechaInicio = moment().format("YYYY-MM-DD");
            this.fechaFinal = moment().format("YYYY-MM-DD");
        }
        else {
            this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
            this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");

        }
        let empleado: number = undefined;
        let cultivo: number = undefined;
        let lote: number = undefined;
        let finca: number = undefined;
        //if (!isNullOrUndefined(this.empleadosSelected) && this.empleadosSelected != 0) {

            empleado = +this.empleadosSelected;
        //}
        //if (!isNullOrUndefined(this.cultivosSelected) && this.cultivosSelected != 0) {
            cultivo = +this.cultivosSelected;
        //}
        //if (!isNullOrUndefined(this.lotesSelected) && this.lotesSelected != 0) {
            lote = +this.lotesSelected;
        //}
        //if (!isNullOrUndefined(this.fincaSelected) && this.fincaSelected != 0) {
            finca = +this.fincaSelected;
        //}*/

        let data = {
            loteId: lote,
            fincaId: finca,
            cultivoId: cultivo,
            empleadoId: empleado,
            fechaInicio: this.fechaInicio,
            fechaFinal: this.fechaFinal,
            agrupar: this.agrupar,
            pagada: this.pagada
        }
        console.log(data)
        this.peticion.post<Cosechas[]>(`cosecha/filtro`, data).subscribe(
            result => {
                Swal.close();
                this.cosechas = result;
                this.$table.bootstrapTable('load', this.cosechas)
            },
            error => {
                Swal.close();
            }
        )
    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#reservation')).daterangepicker(
            function (start, end, label) {
                self.fechaInicio = start;
                self.fechaFinal = end;
            });


        function queryParams() {
            return {
            };
        }
        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
          <span class="badge badge-danger">Sin pagar</span>
          `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-secondary">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        function totalTextFormatter(data) {
            return 'Total'
        }

        function totalCantidadFormatter(data) {
            var field = this.field
            return data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function totalPagoFormatter(data) {
            var field = this.field
            return self.utils.formatMoney(data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0))
        }

        function priceFormatter(value, row, index) {
            return self.utils.formatMoney( value);
        };

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        self.$table = $('#table');
        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable('destroy').bootstrapTable({
            data: self.cosechas,
            pagination: true,
            search: true,
            showFooter: true,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            columns: [
                {
                    field: 'empleadoCodigo',
                    sortable: true,
                    title: 'Codigo'
                },
                {
                    field: 'empleadoNombre',
                    sortable: true,
                    title: 'Empleado',
                    footerFormatter:totalTextFormatter
                }, {
                    field: 'cultivoNombre',
                    sortable: true,
                    title: 'Cultivo'
                },
                {
                    field: 'loteNombre',
                    title: 'Lote'
                },
                {
                    field: 'cultivoPago',
                    sortable: true,
                    formatter: priceFormatter,
                    title: 'Pago'
                },
                {
                    field: 'cantidad',
                    sortable: true,
                    title: 'Cantidad',
                    footerFormatter: totalCantidadFormatter
                },
                {
                    field: 'totalPago',
                    sortable: true,
                    formatter: priceFormatter,
                    footerFormatter: totalPagoFormatter,
                    title: 'Total Pago'
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                },
                {
                    field: 'pagada',
                    title: 'Estado',
                    formatter: "estados",
                    //events: "operateEvents"
                },
            ]
        });
    }

}
