import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporteActividadesComponent } from './reporte-actividades/reporte-actividades.component';
import { ReporteCultivosComponent } from './reporte-cultivos/reporte-cultivos.component';
import { ReporteInversionesComponent } from './reporte-inversiones/reporte-inversiones.component';
import { ReportePlanillaComponent } from './reporte-planilla/reporte-planilla.component';
import { DetallePlanillaCosechaComponent } from './detalle-planilla-cosecha/detalle-planilla-cosecha.component';
import { DetallePlanillaActividadLoteComponent } from './detalle-planilla-actividad-lote/detalle-planilla-actividad-lote.component';
import { DetallePlanillaActividadFincaComponent } from './detalle-planilla-actividad-finca/detalle-planilla-actividad-finca.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';


const routes: Routes = [
  {
    path: 'actividades',
    component: ReporteActividadesComponent
  },
  {
    path: 'cultivos',
    component: ReporteCultivosComponent
  },
  {
    path: 'inversiones',
    component: ReporteInversionesComponent
  },
  {
    path: 'planillas',
    component: ReportePlanillaComponent
  },
  {
    path: 'asistencias',
    component: AsistenciasComponent
  },
  {
    path: 'planillas/detalle',
    component: DetallePlanillaCosechaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
