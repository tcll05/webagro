import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas, Lotes, Cultivos, Planilla } from 'src/app/models/Catalogos';
import { Empleado } from 'src/app/models/Empleados';
import moment from 'moment';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { UtilsService } from 'src/app/Services/utils.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
  selector: 'app-reporte-planilla',
  templateUrl: './reporte-planilla.component.html',
  styleUrls: ['./reporte-planilla.component.css']
})
export class ReportePlanillaComponent implements OnInit {

  tipoOperacionSelected: string = '0';
  empleadosSelected: number = 0;
  fechaInicio: string;
  fechaFinal: string;
  planilla: Planilla[] = [];
  $table: any;
  constructor(private alert:AlertService, private peticion: PeticionService, private router: Router, private utils: UtilsService) { }

  ngOnInit(): void {
    //this.getData();
  }

  getData() {
    if (this.tipoOperacionSelected == '0') {
      this.alert.mensaje(
        'Error',
        "Por favor, seleccione un tipo de operacion",
        'warning'
      )
    }
    else {
      this.showLoading("Cargando datos...")
      this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
      this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
      let params = {
        fechaInicio: this.fechaInicio,
        fechaFinal: this.fechaFinal
      }
      this.peticion.getAll<Planilla[]>(`planilla/filtro/${params.fechaInicio}/${params.fechaFinal}/${this.tipoOperacionSelected}`).subscribe(
        result => {
          Swal.close();
          this.planilla = result;
          this.$table.bootstrapTable('load', this.planilla)
        },
        error => {
          Swal.close();

        }
      )
    }

  }

  showLoading(title) {
    Swal.fire({
      title: title,
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  ngAfterViewInit() {
    var self = this;
    /*(<any>$('.select2bs4')).select2({
      theme: 'bootstrap4'
    });*/
    (<any>$('#reservation')).daterangepicker(function (start, end, label) {
      self.fechaInicio = start;
      self.fechaFinal = end;
    });

    (<any>window).acciones = function (value, row, index) {
      return [
        `<button class="btn btn-block btn-primary btn-sm edit mr-2">Ver detalles</button>`
      ].join('')
    };

    (<any>window).operateEvents = {
      'click .edit': function (e, value, row, index) {
        self.verDetalle(row);//alert('You click like action, row: ' + JSON.stringify(row))
      }
    };



    function tipoOperacion(value, row, index) {
      if (value == "C")
        return "Cosecha";
      if (value == "A")
        return "Actividad por lote";
      else
        return "Actividad por finca";
    };

    function datetimeFormatter(value, row, index) {
      return self.utils.dateTimeFormatter(value)
    };
    function totalTextFormatter(data) {
      return 'Total'
    }

    function totalPagoFormatter(data) {
      var field = this.field
      return self.utils.formatMoney(data.map(function (row) {
        return +row[field]
      }).reduce(function (sum, i) {
        return sum + i
      }, 0))
    }
    function priceFormatter(value, row, index) {
      return self.utils.formatMoney(value);
    };

    self.$table = $('#table');

    (<any>$('#table')).bootstrapTable({
      data:self.planilla,
      pagination: true,
      sortName: 'fechaCreacion',
      sortOrder: 'desc',
      search: true,
      size: 100,
      exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
      locale: 'es-CR',
      showExport: true,
      columns: [
        {
          field: 'codigoEmpleado',
          sortable: true,
          title: 'Codigo'
        },
        {
          field: 'nombreEmpleado',
          title: 'Empleado',
          footerFormatter: totalTextFormatter,
          sortable: true,
        }, {
          field: 'tipoOperacion',
          sortable: true,
          formatter: tipoOperacion,
          title: 'Tipo de operacion'
        }, {
          field: 'metodoPagoNombre',
          sortable: true,
          title: 'Metodo de pago'
        }, {
          field: 'pagoEfectuado',
          title: 'Pago efectuado',
          formatter: priceFormatter,
          footerFormatter: totalPagoFormatter,
        },
        {
          field: 'fechaCreacion',
          formatter: datetimeFormatter,
          title: 'Fecha de pago',
          sortable: true
        },
        {
          field: 'acciones',
          title: 'Acciones',
          width: 120,
          widthUnit: "px",
          formatter: "acciones",
          events: "operateEvents"
        }
      ]
    });
  }

  verDetalle(planilla: Planilla) {
    let selection = {
      planillaId: planilla.planillaId,
      tipo: planilla.tipoOperacion
    }
    sessionStorage.setItem("planilla", JSON.stringify(selection))
    this.router.navigate(['/reportes/planillas/detalle'])

  }

}
