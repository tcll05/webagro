import { Component, OnInit, Input } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { UtilsService } from 'src/app/Services/utils.service';
import { Cosechas } from 'src/app/models/Cosechas';
import moment from 'moment';
import { isNullOrUndefined } from 'util';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { Router } from '@angular/router';

@Component({
    selector: 'app-detalle-planilla-cosecha',
    templateUrl: './detalle-planilla-cosecha.component.html',
    styleUrls: ['./detalle-planilla-cosecha.component.css']
})
export class DetallePlanillaCosechaComponent implements OnInit {
    @Input() fincaSelected: number = 0;
    @Input() lotesSelected: number = 0;

    @Input() fechaInicio: Date;
    @Input() fechaFinal: Date;

    pagar: boolean;

    seleccion: {
        empleadoId: number,
        loteId: number,
        cultivoId: number
    }[] = [];

    cosechas: Cosechas[] = [];
    $table: any;
    constructor(private peticion: PeticionService, private router: Router, private utils: UtilsService) { }

    ngOnInit(): void {
    }

    getCosechas() {
        let data = JSON.parse(sessionStorage.getItem("params"));

        this.peticion.post<Cosechas[]>(`cosecha/filtro`, data).subscribe(
            result => {
                Swal.close();
                this.cosechas = result;
                this.$table.bootstrapTable('load', this.cosechas)
            },
            error => {
                Swal.close();
            }
        )


    }

    ngAfterViewInit() {
        var self = this;

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
      <span class="badge badge-danger">Sin pagar</span>
      `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-success">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        function totalTextFormatter(data) {
            return 'Total'
        }

        function totalCantidadFormatter(data) {
            var field = this.field
            return data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function totalPagoFormatter(data) {
            var field = this.field
            return 'L. ' + data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function priceFormatter(value, row, index) {
            return "L. " + value;
        };

        function queryParams() {
            return {
            };
        }

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        self.$table = $('#table');
        let url = this.peticion.getBaseURL();
        let s = JSON.parse(sessionStorage.getItem("planilla"));
        let id = s.planillaId;
        let tipo: string = ""
        let header: string="";
        let headerTitle: string="Lote";
        if (s.tipo == "C") {
            tipo = "Cultivo";
            header = "loteNombre"
        }
        else if (s.tipo == "A") {
            tipo = "Actividad";
            header = "loteNombre"
        }
        else {
            tipo = "Actividad";
            header = "fincaNombre"
            headerTitle = "Finca"
        }
        (<any>$('#table')).bootstrapTable('destroy').bootstrapTable({
            url: url + "/planilla/" + id,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            pagination: true,
            search: true,
            showFooter: true,
            queryParams: queryParams,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            columns: [
                
                {
                    field: 'empleadoNombre',
                    sortable: true,
                    title: 'Empleado'
                },
                {
                    field: 'nombre',
                    sortable: true,
                    title: `${tipo}`
                },
                {
                    field: `${header}`,
                    title: 'Lote'
                },
                {
                    field: 'totalPago',
                    sortable: true,
                    formatter: priceFormatter,
                    footerFormatter: totalPagoFormatter,
                    title: 'Total Pago'
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                }
            ]
        });
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

}
