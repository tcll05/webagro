import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePlanillaCosechaComponent } from './detalle-planilla-cosecha.component';

describe('DetallePlanillaCosechaComponent', () => {
  let component: DetallePlanillaCosechaComponent;
  let fixture: ComponentFixture<DetallePlanillaCosechaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePlanillaCosechaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePlanillaCosechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
