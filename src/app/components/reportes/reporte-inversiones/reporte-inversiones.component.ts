import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/Services/utils.service';
import { PeticionService } from 'src/app/Services/peticion.service';
import moment from 'moment';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { LoteInversiones, Fincas } from 'src/app/models/Catalogos';

@Component({
  selector: 'app-reporte-inversiones',
  templateUrl: './reporte-inversiones.component.html',
  styleUrls: ['./reporte-inversiones.component.css']
})
export class ReporteInversionesComponent implements OnInit {
  fechaInicio: string;
  fechaFinal: string;
  inversiones:LoteInversiones[]=[];
  fincaSelected:number = 0;
  $table: any;
  fincas:Fincas[]=[];
  constructor(private util: UtilsService, private peticion:PeticionService) { }

  ngOnInit(): void {
    this.getFincas();
  }

  getFincas() {
		this.peticion.getAll<Fincas[]>("finca").subscribe(
			result => {
				this.fincas = result;
			},
			error => {

			}
		)
  }
  
  planillaActividad() {
    this.showLoading("Cargando inversiones...")
		this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
		this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
		let params = {
			fincaId: +this.fincaSelected,
			fechaInicio: this.fechaInicio,
			fechaFinal: this.fechaFinal
		}

    this.peticion.post<LoteInversiones[]>(`lote/inversiones/filtro`, params).subscribe(
      result => {
        Swal.close();
        this.inversiones = result;
        this.$table.bootstrapTable('load', this.inversiones)
      },
      error => {

      }
    )
  }
  showLoading(title) {
    Swal.fire({
      title: title,
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  ngAfterViewInit() {
    var self = this;
    /*(<any>$('.select2bs4')).select2({
      theme: 'bootstrap4'
    });*/
    (<any>$('#reservation')).daterangepicker();
     self.$table = $('#table');
    function queryParams() {
      return {
      };
    }
    function dateFormatter(value, row, index) {
      return self.util.dateTimeFormatter(value);
    };

    let url = this.peticion.getBaseURL();
    (<any>$('#table')).bootstrapTable({
      data: self.inversiones,
      pagination: true,
      search: true,
      size: 100,
      exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
      locale: 'es-CR',
      showExport: true,
      showRefresh: true,
      columns: [
        {
          field: 'fincaNombre',
          title: 'Finca'
        },
        {
          field: 'loteCodigo',
          title: 'Codigo de lote'
        },
        {
          field: 'loteNombre',
          title: 'Lote'
        }, {
          field: 'concepto',
          title: 'Concepto'
        },
        {
          field: 'descripcion',
          title: 'Descripcion'
        },
        {
          field: 'costo',
          title: 'Costo'
        },
        {
          field: 'fechaCreacion',
          formatter:dateFormatter,
          title: 'Fecha'
        },
      ]
    });
  }

}
