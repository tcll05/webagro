import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteInversionesComponent } from './reporte-inversiones.component';

describe('ReporteInversionesComponent', () => {
  let component: ReporteInversionesComponent;
  let fixture: ComponentFixture<ReporteInversionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteInversionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteInversionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
