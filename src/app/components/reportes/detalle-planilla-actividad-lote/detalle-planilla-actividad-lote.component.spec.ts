import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePlanillaActividadLoteComponent } from './detalle-planilla-actividad-lote.component';

describe('DetallePlanillaActividadLoteComponent', () => {
  let component: DetallePlanillaActividadLoteComponent;
  let fixture: ComponentFixture<DetallePlanillaActividadLoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePlanillaActividadLoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePlanillaActividadLoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
