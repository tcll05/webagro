import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    usuario: string;
    contrasenia: string;
    formInicio: FormGroup;
    isLoggedIn$: Observable<boolean>;

    constructor(private alert:AlertService, private peticion: PeticionService, private builder: FormBuilder, private router: Router, private sesion: AuthService) { }

    ngOnInit(): void {
        this.isLoggedIn$ = this.sesion.isLoggedIn;

        this.formInicio = this.builder.group({
            usuario: ['', [Validators.required]],
            contrasenia: ['', [Validators.required]]
        })
    }

    login() {
        if (this.formInicio.invalid) {
            this.formInicio.markAllAsTouched();
            return;
        }
        else {
            this.showLoading("Autenticando...");
            let formData = {
                usuario: this.formInicio.get('usuario').value,
                contrasenia: this.formInicio.get('contrasenia').value,
                origen: "web"
            }
            /*var formData = new FormData();
            formData.append("usuario", this.formInicio.get('usuario').value);
            formData.append("contrasenia", this.formInicio.get('contrasenia').value);
            this.sesion.login("assaaadassasa");
                  this.router.navigate(['/home']);*/
            this.peticion.postLogin<any>("autenticacion", formData).subscribe(
                result => {
                    Swal.close();
                    if (result.resultado == false) {
                        this.alert.mensaje(
                            'Error',
                            result.mensaje,
                            'error'
                        )
                        sessionStorage.removeItem("isLoggedIn");
                    }
                    else {
                        sessionStorage.setItem("user", formData.usuario);
                        this.sesion.login(result.token);
                        this.router.navigate(['/home']);
                    }

                },
                error => {
                    Swal.close();
                    this.alert.mensaje(
                        'Error',
                        error.error.mensaje,
                        'error'
                    )
                    sessionStorage.removeItem('isLoggedIn');
                }
            );
        }
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

}
