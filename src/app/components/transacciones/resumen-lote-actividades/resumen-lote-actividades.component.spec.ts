import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenLoteActividadesComponent } from './resumen-lote-actividades.component';

describe('ResumenLoteActividadesComponent', () => {
  let component: ResumenLoteActividadesComponent;
  let fixture: ComponentFixture<ResumenLoteActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenLoteActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenLoteActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
