import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificarActividadesComponent } from './verificar-actividades.component';

describe('VerificarActividadesComponent', () => {
  let component: VerificarActividadesComponent;
  let fixture: ComponentFixture<VerificarActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
