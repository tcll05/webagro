import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/Services/utils.service';
import { PeticionService } from 'src/app/Services/peticion.service';
import moment from 'moment';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { LoteInversiones, Fincas } from 'src/app/models/Catalogos';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-verificar-actividades',
    templateUrl: './verificar-actividades.component.html',
    styleUrls: ['./verificar-actividades.component.css']
})
export class VerificarActividadesComponent implements OnInit {

    fechaInicio: string;
    fechaFinal: string;
    duplicacion: {
        duplicacionId: number,
        fechaBusqueda: string,
        fechaAsignacion: string,
        tipoActividad: string,
        cantidadActividades: number,
        fechaCreacion: string,
        registros: number[]
    };
    duplicar: any[];
    actividadSelected: number = 0;
    $table: any;
    fincas: Fincas[] = [];
    show: boolean = false;
    message: boolean = false;
    constructor(private alert:AlertService, private util: UtilsService, private peticion: PeticionService) { }

    ngOnInit(): void {
    }

    refreshTable() {
        let self = this;
        function dateFormatter(value, row, index) {
            return self.util.dateTimeFormatter(value);
        };

        function priceFormatter(value, row, index) {
            return self.util.formatMoney(value);
        };

        let campo = ""; let campoNombre = "";
        let campo2 = ""; let campoNombre2 = "";
        if (this.actividadSelected == 2) {
            campo = "loteNombre";
            campoNombre = "Lote";
            campo2 = "loteId";
            campoNombre2 = "Codigo de Lote";
        }
        else {
            campo = "fincaNombre";
            campoNombre = "Finca";
            campo2 = "fincaId";
            campoNombre2 = "Codigo de Finca";
        }
        this.$table.bootstrapTable('refreshOptions', {
            columns: [
                {
                    field: 'state',
                    checkbox: true,
                    align: 'center',
                    valign: 'middle'
                },
                {
                    field: campo2,
                    title: campoNombre2
                },
                {
                    field: campo,
                    title: campoNombre
                }, {
                    field: 'empleadoCodigo',
                    title: 'Codigo de Empleado'
                }, {
                    field: 'empleadoNombre',
                    title: 'Empleado'
                },
                {
                    field: 'actividadNombre',
                    title: 'Nombre de la actividad'
                },
                {
                    field: 'actividadCosto',
                    formatter: priceFormatter,
                    title: 'Costo'
                },
                {
                    field: 'fechaCreacion',
                    formatter: dateFormatter,
                    title: 'Fecha'
                },
            ]
        });
    }

    planillaActividad() {

        this.fechaFinal = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation2').data('daterangepicker').startDate).format("YYYY-MM-DD");
        if (moment(this.fechaFinal).isSameOrAfter(this.fechaInicio) || this.actividadSelected == 0) {
            this.message = true;
        }
        else {
            this.message = false;
            this.showLoading("Cargando inversiones...")
            let actividad: string = "";
            if (this.actividadSelected == 1) actividad = "F"; else actividad = "L";

            this.peticion.getAll<any[]>(`duplicacion/busqueda/${this.fechaFinal}/${actividad}`).subscribe(
                result => {
                    console.log(result);
                    Swal.close();
                    this.duplicar = result;
                    this.cargarTabla();
                    this.refreshTable();
                    this.$table.bootstrapTable('load', this.duplicar)
                },
                error => {

                }
            )
        }
    }
    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

    setDate(){
        $('#reservation2').data('daterangepicker').setStartDate(moment().add(1, "days").format("DD/MM/YYYY HH:mm:ss"));
    }

    cargarTabla() {
        let self = this;
        self.$table = $('#table');
        function queryParams() {
            return {
            };
        }
        function dateFormatter(value, row, index) {
            return self.util.dateTimeFormatter(value);
        };
        let campo = ""; let campoNombre = "";
        let campo2 = ""; let campoNombre2 = "";
        if (this.actividadSelected == 2) {
            campo = "loteNombre";
            campoNombre = "Lote";
            campo2 = "loteId";
            campoNombre2 = "Codigo de Lote";
        }
        else {
            campo = "fincaNombre";
            campoNombre = "Finca";
            campo2 = "fincaId";
            campoNombre2 = "Codigo de Finca";
        }
        function priceFormatter(value, row, index) {
            return self.util.formatMoney(value);
        };

        let url = this.peticion.getBaseURL();
        this.$table.bootstrapTable({
            data: self.duplicar,
            pagination: true,
            search: true,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            showRefresh: true,
            columns: [
                {
                    field: 'state',
                    checkbox: true,
                    align: 'center',
                    valign: 'middle'
                },
                {
                    field: campo2,
                    title: campoNombre2
                },
                {
                    field: campo,
                    title: campoNombre
                }, {
                    field: 'empleadoCodigo',
                    title: 'Codigo de Empleado'
                }, {
                    field: 'empleadoNombre',
                    title: 'Empleado'
                },
                {
                    field: 'actividadNombre',
                    title: 'Nombre de la actividad'
                },
                {
                    field: 'actividadCosto',
                    formatter: priceFormatter,
                    title: 'Costo'
                },
                {
                    field: 'fechaCreacion',
                    formatter: dateFormatter,
                    title: 'Fecha'
                },
            ]
        });

        let $table: any = this.$table;

        $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table',
            function () {
                self.selected($table.bootstrapTable('getSelections').length > 0);
                let registros: number[] = getIdSelections();
                self.duplicacion = {
                    duplicacionId: 0,
                    fechaBusqueda: self.fechaFinal,
                    fechaAsignacion: self.fechaInicio,
                    tipoActividad: self.actividadSelected == 1 ? "F" : "L",
                    cantidadActividades: registros.length,
                    fechaCreacion: moment().format(),
                    registros: registros
                }
                console.log(self.duplicacion);
                // push or splice the selections if you want to save all data selections
            })

        function getIdSelections() {
            return $.map($table.bootstrapTable('getSelections'), function (row) {
                let selection = 0;
                if (self.actividadSelected == 2)
                    selection = row.loteActividadId;
                else selection = row.actividadEmpleadoId;
                return selection;
            })
        }
    }

    selected(state: boolean) {

        this.show = state;
    }

    duplicarActividades() {
        if (moment(this.fechaFinal).isSameOrAfter(this.fechaInicio) || this.actividadSelected == 0) {
            this.message = true;
        }
        else {


            this.showLoading("Duplicando actividades seleccionadas...")
            this.peticion.post<any>(`duplicacion`, this.duplicacion).subscribe(
                result => {
                    Swal.close();
                    if (result.resultado) {
                        this.alert.mensaje(
                            'Correcto',
                            'Actividades duplicadas exitosamente!',
                            'success'
                        );
                        this.duplicar = [];
                        this.show = false;
                        this.message = false;
                        this.$table.bootstrapTable('load', this.duplicar)
                        //this.getActividadEmpleado()
                        //this.$table.bootstrapTable('load', this.cosechas);
                    }
                    else {
                        this.alert.mensaje(
                            'Error',
                            result.mensaje,
                            'error'
                        )
                    }
                },
                error => {
                    this.alert.mensaje(
                        'Error',
                        'Ocurrio un error al intentar duplicar las actividades',
                        'error'
                    )
                }
            )
        }
    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#reservation')).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        (<any>$('#reservation2')).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        this.setDate();
        this.cargarTabla();
    }

    selectAll() {
        (<any>$('#table')).bootstrapTable('togglePagination').bootstrapTable('checkAll').bootstrapTable('togglePagination');
    }

}
