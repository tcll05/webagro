import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAsignacionActividadesLoteComponent } from './lista-asignacion-actividades-lote.component';

describe('ListaAsignacionActividadesLoteComponent', () => {
  let component: ListaAsignacionActividadesLoteComponent;
  let fixture: ComponentFixture<ListaAsignacionActividadesLoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAsignacionActividadesLoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAsignacionActividadesLoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
