import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { UtilsService } from 'src/app/Services/utils.service';
import { Router } from '@angular/router';
import { ActividadLote } from 'src/app/models/Catalogos';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import moment from 'moment';
import { isNullOrUndefined } from 'util';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-lista-asignacion-actividades-lote',
    templateUrl: './lista-asignacion-actividades-lote.component.html',
    styleUrls: ['./lista-asignacion-actividades-lote.component.css']
})
export class ListaAsignacionActividadesLoteComponent implements OnInit {

    constructor(private alert:AlertService, private modalService: NgbModal, private peticion: PeticionService, private utils: UtilsService, private router: Router) { }
    $table: any;
    actividades: ActividadLote[];
    fechaInicio: string;
    fechaFinal: string;
    pagada: boolean = false;

    ngOnInit(): void {
        sessionStorage.removeItem("actividadLote");
    }

    getData() {
        this.loading("Cargando datos...")
        this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");

        this.peticion.getAll<any>(`/lote/actividades/${this.fechaInicio}/${this.fechaFinal}/${this.pagada}`).subscribe(
            result => {
                Swal.close();
                if (result.resultado == true) {
                    this.actividades = result.informacion;
                    localStorage.setItem("ActividadesLote", JSON.stringify(this.actividades));
                    this.$table.bootstrapTable('load', this.actividades);
                }
                else {

                    this.alert.mensaje(
                        'Error!',
                        result.mensaje,
                        'error'
                    );
                }

            },
            error => {
                Swal.close();
                this.alert.mensaje(
                    'Error!',
                    error.error.mensaje,
                    'error'
                );
            }
        )
    }

    ngAfterViewInit() {
        var self = this;
        (<any>$('#reservation')).daterangepicker();
        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
                `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
            ].join('')
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                self.delete(row);//alert('You click like action, row: ' + JSON.stringify(row))
            }
        };

        (<any>window).pagadaFormat = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
              <span class="badge badge-danger">Sin pagar</span>
              `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-success">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).numberFormat = function (value, row, index) {
            console.log(value);
            return self.utils.formatMoney(value);
        };

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        self.$table = $('#table');
        (<any>$('#table')).bootstrapTable({
            data: self.actividades,
            pagination: true,
            search: true,
            queryParams: queryParams,
            showRefresh: true,
            sortName: 'fechaCreacion',
            sortOrder: 'desc',
            size: 25,
            locale: 'es-CR',
            columns: [
                {
                    field: 'loteNombre',
                    sortable: true,
                    title: 'Lote'
                },
                {
                    field: 'empleadoNombre',
                    sortable: true,
                    title: 'Empleado'
                }, {
                    field: 'actividadNombre',
                    sortable: true,
                    title: 'Actividad'
                },
                {
                    field: 'actividadCosto',
                    sortable: true,
                    title: 'Costo',
                    formatter: "numberFormat"
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                },
                {
                    field: 'pagada',
                    sortable: true,
                    formatter: "pagadaFormat",
                    title: 'Estado'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 190,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]
        });
        self.actividades = JSON.parse(localStorage.getItem("ActividadesLote"));
        if(!isNullOrUndefined(this.actividades))
        {
            self.$table.bootstrapTable('load', this.actividades);
        }
    }

    editar(actividad: ActividadLote) {
        if (!actividad.pagada) {
            sessionStorage.setItem("actividadLote", JSON.stringify(actividad));
            this.router.navigate(['transacciones/actividadesLote/crear'])
        }
        else {
            this.alert.mensaje(
                'Error!',
                'El registro no se puede modificar porque ya ha sido pagado.',
                'warning'
            );
        }
    }

    delete(actividad: ActividadLote) {
        if (!actividad.pagada) {
            Swal.fire({
                title: 'Eliminar Registro',
                text: `Seguro desea eliminar la actividad de ${actividad.empleadoNombre}?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    this.peticion.delete<any>("lote/actividades", actividad.loteActividadId).subscribe(
                        result => {
                            if (result.resultado) {
                                this.alert.mensaje(
                                    'Correcto!',
                                    'El registro fue eliminado correctamente.',
                                    'success'
                                );
                                this.$table.bootstrapTable('refresh');
                            }
                            else {
                                this.alert.mensaje(
                                    'Error!',
                                    result.mensaje,
                                    'warning'
                                );
                            }
                        },
                        error => {
                            this.alert.mensaje(
                                'Error!',
                                error,
                                'warning'
                            );
                        }
                    )

                } else if (result.dismiss === Swal.DismissReason.cancel) {
                }
            });

        }
        else {
            this.alert.mensaje(
                'Error!',
                'El registro no se puede eliminar porque ya ha sido pagado.',
                'warning'
            );
        }
    }

    loading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    }
}
