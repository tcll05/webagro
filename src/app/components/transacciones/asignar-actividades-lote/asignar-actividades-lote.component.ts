import { Component, OnInit } from '@angular/core';
import { Fincas, Actividades, ActividadLote, Lotes } from 'src/app/models/Catalogos';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { isNullOrUndefined } from 'util';
import { BuscarEmpleadosComponent } from 'src/app/shared/buscar-empleados/buscar-empleados.component';
import { forkJoin } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import moment from 'moment';
import { AlertService } from 'src/app/Services/alert.service';
@Component({
    selector: 'app-asignar-actividades-lote',
    templateUrl: './asignar-actividades-lote.component.html',
    styleUrls: ['./asignar-actividades-lote.component.css']
})
export class AsignarActividadesLoteComponent implements OnInit {
    fincas: Fincas[] = [];
    c: boolean = false;
    actividades: Actividades[] = [];
    lotesAll: Lotes[] = [];
    lotes: Lotes[] = [];
    fincaSelected: number = 0;
    nombreEmpleado: string;
    saveActividad: ActividadLote;
    tmpActividad: ActividadLote;
    formActividad: FormGroup;
    jornada: number = 1;
    selectedJornada:number = 1;
    actividadHoras:number=0;
    index:number=0;
    constructor(private alert:AlertService, private formBuilder: FormBuilder, private modalService: NgbModal, private peticion: PeticionService) { }

    ngOnInit(): void {
        this.tmpActividad = JSON.parse(sessionStorage.getItem("actividadLote"));

        this.getCatalogos();
        this.formActividad = this.formBuilder.group({
            fincaId: [0, [Validators.required]],
            empleadoId: [0, [Validators.required]],
            actividadId: [0, [Validators.required]],
            loteId: [0, [Validators.required]],
            empleadoNombre: [null, [Validators.required]],
            fechaCreacion:[null]
        })
        if (!isNullOrUndefined(this.tmpActividad)) {
            this.formActividad.patchValue(this.tmpActividad);
            if(this.tmpActividad.actividadHoras > 0)
            {
                this.selectedJornada = 2;
                this.changeGender(2);
                this.actividadHoras = this.tmpActividad.actividadHoras;
            }
            this.formActividad.patchValue(
                {
                    "fechaCreacion":moment(this.tmpActividad.fechaCreacion).format("DD/MM/YYYY")
                }
            );
            (<any>$('#fechaCreacion')).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
            $('#fechaCreacion').data('daterangepicker').setStartDate(moment(this.tmpActividad.fechaCreacion).format("DD/MM/YYYY"));
        }
    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#fechaCreacion')).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    }

    getFincaSelected() {
        let tmpLote: Lotes = this.lotesAll.find(x => x.loteId == this.tmpActividad.loteId)
        this.fincas.forEach(finca => {
            if (finca.fincaId == tmpLote.fincaId) {
                this.fincaSelected = finca.fincaId;
                this.getLotes(this.fincaSelected);
                this.formActividad.patchValue({
                    "fincaId": finca.fincaId,
                    "loteId": tmpLote.loteId
                })
                return;
            }
        });

    }
    buscarEmpleado() {
        const modalRef = this.modalService.open(BuscarEmpleadosComponent, { scrollable: true, size: 'lg' });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                console.log(receivedEntry)
                this.formActividad.patchValue({
                    "empleadoId": receivedEntry.empleadoId,
                    "empleadoNombre": `${receivedEntry.nombres} ${receivedEntry.apellidos}`
                });
                //this.nombreEmpleado=`${receivedEntry.nombres} ${receivedEntry.apellidos}`;
            })
    }

    validarFecha() {
        let dias: number = moment($('#fechaCreacion').data('daterangepicker').startDate).diff(5, "days");
    }


    changeGender(e) {
        this.selectedJornada=e;
        if(e == 1){
            this.actividadHoras = 0;
        }
        //console.log(e.target.value);
    }

    guardar() {
        this.loading("Guardando actividad...");
        if (this.formActividad.invalid) {
            this.formActividad.markAllAsTouched();
            return;
        }
        else if (this.validarActividad() || this.validarFinca() || this.validarLote()) {
            this.formActividad.markAllAsTouched();
            return;
        }
        else {
            this.saveActividad = this.formActividad.value;
            if (this.tmpActividad != null)
                this.saveActividad.loteActividadId = +this.tmpActividad.loteActividadId;

            this.saveActividad.empleadoId = +this.saveActividad.empleadoId;
            this.saveActividad.actividadId = +this.saveActividad.actividadId;
            this.saveActividad.fincaId = +this.saveActividad.fincaId;
            this.saveActividad.loteId = +this.saveActividad.loteId;
            this.saveActividad.actividadHoras = +this.actividadHoras;

            let ff = moment($('#fechaCreacion').data('daterangepicker').startDate).format('YYYY-MM-DD')+'T'+moment().format("HH:mm:ss")

            this.saveActividad.fechaCreacion = ff;
            
            let request = null;
            if (isNullOrUndefined(this.tmpActividad))
                request = this.peticion.post<any>("lote/actividades", this.saveActividad);
            else
                request = this.peticion.put<any>("lote/actividades", this.saveActividad, this.saveActividad.loteActividadId);
            forkJoin([request]).subscribe(
                result => {
                    Swal.close();
                    let result2: any = result;
                    if (result2[0].resultado == true) { //si el resultado es correcto
                        
                        sessionStorage.removeItem("actividadLote")
                        this.tmpActividad = undefined;
                        this.formActividad.reset();
                        this.changeGender(1);
                        this.alert.mensaje(
                            'Registro agregado!',
                            'El registro ha sido creado con exito',
                            'success'
                        );
                        (<any>$('#table')).bootstrapTable('refresh');
                        $('#fechaCreacion').data('daterangepicker').setStartDate(moment().format("DD/MM/YYYY"));
                    }
                    else {
                        this.alert.mensaje(
                            'Error!',
                            result2.mensaje,
                            'error'
                        )
                    }

                },
                error => {
                    this.alert.mensaje(
                        'Error!',
                        error.error.mensaje,
                        'error'
                    )
                }
            )
        }
    }

    getCatalogos() {
        this.loading("Cargando datos de catalogos...")
        const fincas = this.peticion.getAll<Fincas[]>("finca");
        const actividades = this.peticion.getAll<Actividades[]>("actividad");
        const lotes = this.peticion.getAll<Lotes[]>("lote");
        forkJoin([fincas, actividades, lotes]).subscribe(
            result => {

                result[0].forEach(finca => {
                    if (finca.activo) {
                        this.fincas.push(finca)
                    }
                });
                result[1].forEach(actividad => {
                    if (actividad.activo) {
                        this.actividades.push(actividad);
                    }
                });
                result[2].forEach(lote => {
                    if (lote.activo) {
                        this.lotesAll.push(lote);
                    }
                });
                if (!isNullOrUndefined(this.tmpActividad)) {
                    this.getFincaSelected();
                }
                Swal.close();
            },
            error => {
                Swal.close();
                this.alert.mensaje(
                    'Error!',
                    "Ha ocurrido un error al cargar los catalogos...",
                    'error'
                )
            }
        )
    }

    getLotes(fincaId: number) {
        this.lotes = [];
        this.lotesAll.forEach(lote => {
            if (lote.fincaId == fincaId) {
                this.lotes.push(lote);
            }
        });
    }

    loading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    }

    validarFinca() {
        if (this.formActividad.get('fincaId').value == 0)
            return true;
        else return false;
    }

    validarLote() {
        if (this.formActividad.get('loteId').value == 0)
            return true;
        else return false;
    }

    validarActividad() {
        if (this.formActividad.get('actividadId').value == 0)
            return true;
        else return false;
    }

}
