import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarActividadesLoteComponent } from './asignar-actividades-lote.component';

describe('AsignarActividadesLoteComponent', () => {
  let component: AsignarActividadesLoteComponent;
  let fixture: ComponentFixture<AsignarActividadesLoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarActividadesLoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarActividadesLoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
