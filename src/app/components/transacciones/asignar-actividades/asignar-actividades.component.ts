import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuscarEmpleadosComponent } from 'src/app/shared/buscar-empleados/buscar-empleados.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas, Actividades, ActividadEmpleado } from 'src/app/models/Catalogos';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { forkJoin } from 'rxjs';
import { isNullOrUndefined } from 'util';
import moment from 'moment';
import { debug } from 'console';
import { AlertService } from 'src/app/Services/alert.service';
@Component({
    selector: 'app-asignar-actividades',
    templateUrl: './asignar-actividades.component.html',
    styleUrls: ['./asignar-actividades.component.css']
})
export class AsignarActividadesComponent implements OnInit {
    fincas: Fincas[] = [];
    actividades: Actividades[] = [];
    nombreEmpleado: string;
    saveActividad: ActividadEmpleado;
    tmpActividad: ActividadEmpleado;
    formActividad: FormGroup;
    jornada: number = 1;
    selectedJornada:number = 1;
    actividadHoras:number=0;
    constructor(private alert:AlertService, private formBuilder: FormBuilder, private modalService: NgbModal, private peticion: PeticionService) { }

    ngOnInit(): void {
        this.tmpActividad = JSON.parse(sessionStorage.getItem("actividadEmpleado"));

        this.getCatalogos();
        this.formActividad = this.formBuilder.group({
            fincaId: [0, [Validators.required]],
            empleadoId: [0, [Validators.required]],
            actividadId: [0, [Validators.required]],
            empleadoNombre: [null, [Validators.required]],
            fechaCreacion: [null],
            actividadesHora: [0],
        })
        if (!isNullOrUndefined(this.tmpActividad)) {

            this.formActividad.patchValue(this.tmpActividad);
            if(this.tmpActividad.actividadHoras > 0)
            {
                this.selectedJornada = 2;
                this.changeGender(2);
                this.actividadHoras = this.tmpActividad.actividadHoras;
            }
            this.formActividad.patchValue(
                {
                    "fechaCreacion": moment(this.tmpActividad.fechaCreacion).format("DD/MM/YYYY")
                }
            );
            (<any>$('#fechaCreacion')).daterangepicker({
                timePicker: true,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY HH:mm:ss'
                }
            });
            $('#fechaCreacion').data('daterangepicker').setStartDate(moment(this.tmpActividad.fechaCreacion).format("DD/MM/YYYY HH:mm:ss"));
        }

        //debugger;
        console.log(moment().format("HH:mm:ss"));
    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#fechaCreacion')).daterangepicker({
            timePicker: false,
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    }

    buscarEmpleado() {
        const modalRef = this.modalService.open(BuscarEmpleadosComponent, { scrollable: true, size: 'lg' });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                console.log(receivedEntry)
                this.formActividad.patchValue({
                    "empleadoId": receivedEntry.empleadoId,
                    "empleadoNombre": `${receivedEntry.nombres} ${receivedEntry.apellidos}`
                });
                //this.nombreEmpleado=`${receivedEntry.nombres} ${receivedEntry.apellidos}`;
            })
    }

    validarFecha() {
        let dias: number = moment($('#fechaCreacion').data('daterangepicker').startDate).diff(5, "days");
    }


    changeGender(e) {
        this.selectedJornada=e;
        if(e == 1){
            this.actividadHoras = 0;
        }
        //console.log(e.target.value);
    }

    guardar() {
        this.loading("Guardando actividad...");
        if (this.formActividad.invalid) {

            this.formActividad.markAllAsTouched();
            return;
        }
        else if (this.validarActividad() || this.validarFinca()) {
            this.formActividad.markAllAsTouched();
            return;
        }
        else {
            this.saveActividad = this.formActividad.value;
            if (this.tmpActividad != null)
                this.saveActividad.actividadEmpleadoId = +this.tmpActividad.actividadEmpleadoId;

            this.saveActividad.empleadoId = +this.saveActividad.empleadoId;
            this.saveActividad.actividadId = +this.saveActividad.actividadId;
            this.saveActividad.fincaId = +this.saveActividad.fincaId;
            this.saveActividad.actividadHoras = +this.actividadHoras;

            let ff = moment($('#fechaCreacion').data('daterangepicker').startDate).format('YYYY-MM-DD') + 'T' + moment().format("HH:mm:ss")

            this.saveActividad.fechaCreacion = ff;

            let request = null;
            if (isNullOrUndefined(this.tmpActividad))
                request = this.peticion.post<any>("actividad/empleado", this.saveActividad);
            else
                request = this.peticion.put<any>("actividad/empleado", this.saveActividad, this.saveActividad.actividadEmpleadoId);
            forkJoin([request]).subscribe(
                result => {
                    Swal.close();
                    let result2: any = result;
                    if (result2[0].resultado == true) { //si el resultado es correcto
                        sessionStorage.removeItem("actividadEmpleado")
                        this.tmpActividad = undefined;
                        this.formActividad.reset();
                        this.changeGender(1);
                        this.alert.mensaje(
                            'Registro agregado!',
                            'El registro ha sido creado con exito',
                            'success'
                        );
                        (<any>$('#table')).bootstrapTable('refresh');
                        $('#fechaCreacion').data('daterangepicker').setStartDate(moment().format("DD/MM/YYYY"));
                    }
                    else {
                        Swal.close();
                        this.alert.mensaje(
                            'Error!',
                            result2.mensaje,
                            'error'
                        )
                    }

                },
                error => {
                    this.alert.mensaje(
                        'Error!',
                        'Ha ocurrido un error al crear el registro',
                        'error'
                    )
                }
            )
        }
    }

    getCatalogos() {
        this.loading("Cargando datos de catalogos...")
        const fincas = this.peticion.getAll<Fincas[]>("finca");
        const actividades = this.peticion.getAll<Actividades[]>("actividad");
        forkJoin([fincas, actividades]).subscribe(
            result => {

                result[0].forEach(finca => {
                    if (finca.activo) {
                        this.fincas.push(finca)
                    }
                });
                result[1].forEach(actividad => {
                    if (actividad.activo) {
                        this.actividades.push(actividad);
                    }
                });
                Swal.close();
            },
            error => {
                Swal.close();
                this.alert.mensaje(
                    'Error!',
                    "Ha ocurrido un error al cargar los catalogos...",
                    'error'
                )
            }
        )
    }

    loading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    }

    validarFinca() {
        if (this.formActividad.get('fincaId').value == 0)
            return true;
        else return false;
    }

    validarActividad() {
        if (this.formActividad.get('actividadId').value == 0)
            return true;
        else return false;
    }

}
