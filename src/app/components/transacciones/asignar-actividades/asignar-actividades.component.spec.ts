import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarActividadesComponent } from './asignar-actividades.component';

describe('AsignarActividadesComponent', () => {
  let component: AsignarActividadesComponent;
  let fixture: ComponentFixture<AsignarActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
