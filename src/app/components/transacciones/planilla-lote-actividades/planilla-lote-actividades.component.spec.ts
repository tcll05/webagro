import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanillaLoteActividadesComponent } from './planilla-lote-actividades.component';

describe('PlanillaLoteActividadesComponent', () => {
  let component: PlanillaLoteActividadesComponent;
  let fixture: ComponentFixture<PlanillaLoteActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanillaLoteActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanillaLoteActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
