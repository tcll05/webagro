import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAsignacionActividadesComponent } from './lista-asignacion-actividades.component';

describe('ListaAsignacionActividadesComponent', () => {
  let component: ListaAsignacionActividadesComponent;
  let fixture: ComponentFixture<ListaAsignacionActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAsignacionActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAsignacionActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
