import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { UtilsService } from 'src/app/Services/utils.service';
import { ActividadEmpleado } from 'src/app/models/Catalogos';
import { AsignarActividadesComponent } from '../asignar-actividades/asignar-actividades.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import moment from 'moment';
import { isNullOrUndefined } from 'util';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-lista-asignacion-actividades',
    templateUrl: './lista-asignacion-actividades.component.html',
    styleUrls: ['./lista-asignacion-actividades.component.css']
})
export class ListaAsignacionActividadesComponent implements OnInit {

    constructor(private alert:AlertService, private modalService: NgbModal, private peticion: PeticionService, private utils: UtilsService, private router: Router) { }
    $table: any;
    actividades: ActividadEmpleado[];
    fechaInicio: string;
    fechaFinal: string;
    pagada: boolean = false;

    ngOnInit(): void {
        sessionStorage.removeItem("actividadEmpleado");
    }

    getData() {
        this.loading("Cargando datos...")
        this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");

        this.peticion.getAll<any>(`/actividad/empleado/${this.fechaInicio}/${this.fechaFinal}/${this.pagada}`).subscribe(
            result => {
                Swal.close();
                if (result.resultado == true) {
                    this.actividades = result.informacion;
                    localStorage.setItem("ActividadesEmpleado", JSON.stringify(this.actividades));
                    this.$table.bootstrapTable('load', this.actividades);
                }
                else {
                    
                    this.alert.mensaje(
                        'Error!',
                        result.mensaje,
                        'error'
                    );
                }

            },
            error => {
                Swal.close();
                this.alert.mensaje(
                    'Error!',
                    error.error.mensaje,
                    'error'
                );
            }
        )
    }

    ngAfterViewInit() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`,
                `<button class="btn btn-outline-danger btn-sm delete"><i class="fas fa-trash"></i>&nbsp;Eliminar</button>`
            ].join('')
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                self.delete(row);//alert('You click like action, row: ' + JSON.stringify(row))
            }
        };

        (<any>window).numberFormat = function (value, row, index) {
            return self.utils.formatMoney(value);
        };

        (<any>window).pagadaFormat = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
              <span class="badge badge-danger">Sin pagar</span>
              `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-success">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        (<any>$('#reservation')).daterangepicker();
        function queryParams() {
            return {
            };
        }
        self.$table = $('#table');

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            data: self.actividades,
            pagination: true,
            search: true,
            queryParams: queryParams,
            sortable: true,
            showRefresh: true,
            sortName: 'fechaCreacion',
            sortOrder: 'desc',
            size: 25,
            locale: 'es-CR',
            columns: [
                {
                    field: 'fincaNombre',
                    sortable: true,
                    title: 'Finca'
                },
                {
                    field: 'empleadoNombre',
                    sortable: true,
                    title: 'Empleado'
                }, {
                    field: 'actividadNombre',
                    sortable: true,
                    title: 'Actividad'
                },
                {
                    field: 'actividadCosto',
                    sortable: true,
                    formatter: "numberFormat",
                    title: 'Costo'
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                },
                {
                    field: 'pagada',
                    sortable: true,
                    formatter: "pagadaFormat",
                    title: 'Estado'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 190,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]
        });
        self.actividades = JSON.parse(localStorage.getItem("ActividadesEmpleado"));
        if(!isNullOrUndefined(this.actividades))
        {
            self.$table.bootstrapTable('load', this.actividades);
        }
    }

    editar(actividad: ActividadEmpleado) {
        if (!actividad.pagada) {
            sessionStorage.setItem("actividadEmpleado", JSON.stringify(actividad));
            this.router.navigate(['transacciones/actividades/crear'])
        }
        else {
            this.alert.mensaje(
                'Error!',
                'El registro no se puede modificar porque ya ha sido pagado.',
                'warning'
            );
        }

    }

    delete(actividad: ActividadEmpleado) {
        
        if (!actividad.pagada) {
            Swal.fire({
                title: 'Eliminar Registro',
                text: `Seguro desea eliminar la actividad de ${actividad.empleadoNombre}?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    this.loading("Eliminando actividad...");
                    this.peticion.delete<any>("actividad/empleado", actividad.actividadEmpleadoId).subscribe(
                        result => {
                            Swal.close();
                            if (result.resultado) {
                                this.alert.mensaje(
                                    'Correcto!',
                                    'El registro fue eliminado correctamente.',
                                    'success'
                                );
                                this.$table.bootstrapTable('refresh');
                            }
                            else {
                                this.alert.mensaje(
                                    'Error!',
                                    result.mensaje,
                                    'warning'
                                );
                            }
                        },
                        error => {
                            Swal.close();
                            this.alert.mensaje(
                                'Error!',
                                error,
                                'warning'
                            );
                        }
                    )

                } else if (result.dismiss === Swal.DismissReason.cancel) {
                }
            });

        }
        else {
            this.alert.mensaje(
                'Error!',
                'El registro no se puede eliminar porque ya ha sido pagado.',
                'warning'
            );
        }
    }

    loading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    }

}
