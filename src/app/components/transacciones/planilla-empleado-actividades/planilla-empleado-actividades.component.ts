import { Component, OnInit, Input } from '@angular/core';

import Swal from "sweetalert2/dist/sweetalert2.js";
import { ActividadEmpleado } from 'src/app/models/Catalogos';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/Services/utils.service';
import moment from 'moment';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-planilla-empleado-actividades',
    templateUrl: './planilla-empleado-actividades.component.html',
    styleUrls: ['./planilla-empleado-actividades.component.css']
})
export class PlanillaEmpleadoActividadesComponent implements OnInit {
    @Input() fechaInicio: Date;
    @Input() fechaFinal: Date;
    pagar: boolean;
    reporte: boolean = false;

    rs = "";
    viewImporte = false;
    total: number = 0;

    seleccion: {
        empleadoId: number,
        fincaId: number,
        actividadId: number,
        totalPago: number
    }[] = [];

    cosechas: ActividadEmpleado[] = [];
    $table: any;
    constructor(private alert:AlertService, private peticion: PeticionService, private router: Router, private utils: UtilsService) { }

    ngOnInit(): void {

        this.getActividadEmpleado();
    }

    getActividadEmpleado() {

        this.showLoading("Cargando actividades...")
        let data = JSON.parse(sessionStorage.getItem("params"));
        this.reporte = data.reporte;
        /*if (data.loteId !== 0 && data.reporte)
          this.router.navigate(["/transacciones/planillas/actividadLote"]);
    
        if (data.fincaId == 0) {
          this.alert.mensaje(
            'Aviso',
            'Por favor complete los campos para generar la planilla!',
            'warning'
          )
          this.router.navigate(['/transacciones/planillas'])
        }
        else {*/
        console.log(data)
        this.peticion.post<ActividadEmpleado[]>(`actividad/empleado/filtro`, data).subscribe(
            result => {
                Swal.close();
                this.cosechas = result;
                this.$table.bootstrapTable('load', this.cosechas)
            },
            error => {

            }
        )
        //}

    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#reservation')).daterangepicker(
            function (start, end, label) {
                self.fechaInicio = start;
                self.fechaFinal = end;
            });


        function queryParams() {
            return {
            };
        }
        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
      <span class="badge badge-danger">Sin pagar</span>
      `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-secondary">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        function totalTextFormatter(data) {
            return 'Total'
        }

        function totalCantidadFormatter(data) {
            var field = this.field
            return data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function totalPagoFormatter(data) {
            var field = this.field
            return self.utils.formatMoney(data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0))
        }

        function priceFormatter(value, row, index) {
            return self.utils.formatMoney(value);
        };

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        function detail(index, row) {
            return self.utils.getImporte(row.actividadCosto);
        }
        self.$table = $('#table');
        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable('destroy').bootstrapTable({
            data: self.cosechas,
            pagination: true,
            search: true,
            showFooter: true,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            detailView: true,
            detailFormatter: detail,
            columns: [
                {
                    field: 'state',
                    checkbox: true,
                    align: 'center',
                    valign: 'middle'
                },
                {
                    field: 'fincaNombre',
                    footerFormatter: totalTextFormatter,
                    title: 'Finca'
                },
                {
                    field: 'empleadoCodigo',
                    sortable: true,
                    title: 'Codigo'
                },
                {
                    field: 'empleadoNombre',
                    title: 'Empleado'
                }, {
                    field: 'actividadNombre',
                    title: 'Actividad'
                },
                {
                    field: 'actividadCosto',
                    title: 'Costo',
                    formatter: priceFormatter,
                    footerFormatter: totalPagoFormatter,
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                }
            ]
        });
        let $table: any = $('#table');

        $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table',
            function () {
                self.selected($table.bootstrapTable('getSelections').length > 0);
                self.seleccion = getIdSelections();

                // push or splice the selections if you want to save all data selections
            })

        function getIdSelections() {
            return $.map($table.bootstrapTable('getSelections'), function (row) {
                let selection = {
                    empleadoId: row.empleadoId,
                    fincaId: row.fincaId,
                    actividadId: row.actividadId,
                    totalPago: row.actividadCosto
                }
                return selection;
            })
        }
    }

    getImporte() {
        this.rs = "";
        let total: {
            cantidad: number,
            moneda: number
        }[] = [];
        this.seleccion.forEach(element => {
            let importe = this.utils.getArrayImporte(element.totalPago);
            importe.forEach(element3 => {
                if (this.existe(total, element3.moneda)) {
                    total.forEach(element2 => {
                        if (element2.moneda == element3.moneda)
                            element2.cantidad += element3.cantidad;
                    });
                }
                else {
                    total.push({
                        cantidad: element3.cantidad,
                        moneda: element3.moneda
                    })
                }

            });
        });
        total.sort(function (a, b) { return b.moneda - a.moneda });
        this.total = 0;
        this.viewImporte = true;
        this.rs = "<ul>";
        total.forEach(element => {
            this.rs += "<li>" + element.cantidad + " billetes de: " + this.utils.formatMoney(element.moneda) + "</li>";
            this.total += (element.moneda * element.cantidad);
        });
        this.rs += "</ul>";
    }

    selectAll(){
        (<any>$('#table')).bootstrapTable('togglePagination').bootstrapTable('checkAll').bootstrapTable('togglePagination');
    }

    existe(a: any[], n1) {
        let exists: boolean = false;
        a.forEach(element => {
            if (element.moneda == n1)
                exists = true;

        });
        return exists;
    }

    pagarPlanilla() {
        this.showLoading("Pagando planilla seleccionada...")
        let data = JSON.parse(sessionStorage.getItem("params"));
        let finicio = moment(data.fechaInicio).format("YYYY-MM-DD");
        let ffinal = moment(data.fechaFinal).format("YYYY-MM-DD");

        this.peticion.post<any>(`actividad/empleado/pago/${finicio}/${ffinal}`, this.seleccion).subscribe(
            result => {
                Swal.close();
                if (result.resultado) {
                    this.alert.mensaje(
                        'Correcto',
                        'Planilla pagada exitosamente!',
                        'success'
                    );
                    this.router.navigate(['transacciones/planillas']);
                    //this.getActividadEmpleado()
                    //this.$table.bootstrapTable('load', this.cosechas);
                }
                else {
                    this.alert.mensaje(
                        'Error',
                        result.mensaje,
                        'error'
                    )
                }
            },
            error => {
                this.alert.mensaje(
                    'Error',
                    'Ocurrio un error al intentar pagar la planilla',
                    'error'
                )
            }
        )
    }

    selected(state: boolean) {

        this.pagar = state;
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

}
