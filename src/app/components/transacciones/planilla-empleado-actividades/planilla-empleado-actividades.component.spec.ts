import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanillaEmpleadoActividadesComponent } from './planilla-empleado-actividades.component';

describe('PlanillaEmpleadoActividadesComponent', () => {
  let component: PlanillaEmpleadoActividadesComponent;
  let fixture: ComponentFixture<PlanillaEmpleadoActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanillaEmpleadoActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanillaEmpleadoActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
