import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas, Lotes } from 'src/app/models/Catalogos';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import moment from 'moment';

@Component({
  selector: 'app-generar-resumen',
  templateUrl: './generar-resumen.component.html',
  styleUrls: ['./generar-resumen.component.css']
})
export class GenerarResumenComponent implements OnInit {

  fincaSelected: number = 0;
  lotesSelected: number = 0;

  fincas: Fincas[] = [];
  lotes: Lotes[] = [];

  fechaInicio: string;
  fechaFinal: string;
  constructor(private peticion: PeticionService, private router: Router) { }

  ngOnInit(): void {
    this.getFincas();
  }

  ngAfterViewInit() {
    var self = this;
    /*(<any>$('.select2bs4')).select2({
      theme: 'bootstrap4'
});*/

    (<any>$('#reservation')).daterangepicker(
      function (start, end, label) {
        self.fechaInicio = start;
        self.fechaFinal = end;
      });
  }

  planillaCultivos() {
    this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
    this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
    let params = {
      fincaId: +this.fincaSelected,
      loteId: +this.lotesSelected,
      fechaInicio: this.fechaInicio,
      fechaFinal: this.fechaFinal,
      agrupar: true,
      pagada: false
    }

    sessionStorage.setItem("params", JSON.stringify(params));
    // this.router.navigate(["/cosechas"]);
  }

  planillaActividad() {
    this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
    this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
    let params = {
      fincaId: +this.fincaSelected,
      loteId: +this.lotesSelected,
      fechaInicio: this.fechaInicio,
      fechaFinal: this.fechaFinal,
      agrupar: true,
      pagada: false
    }

    sessionStorage.setItem("params", JSON.stringify(params));
  }

  getFincas() {
    this.peticion.getAll<Fincas[]>("finca").subscribe(
      result => {
        this.fincas = result;
      },
      error => {

      }
    )
  }

  getLotes(finca: Number) {
    //let fincaId = this.fincaSelected.fincaId 
    console.log(finca)
    if (!isNullOrUndefined(finca)) {
      this.peticion.getAll<Lotes[]>(`lote/finca/${finca}`).subscribe(
        result => {
          this.lotes = result;
        },
        error => {

        }
      )
    }

  }

}
