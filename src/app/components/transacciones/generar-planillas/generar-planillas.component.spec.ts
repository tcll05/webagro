import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarPlanillasComponent } from './generar-planillas.component';

describe('GenerarPlanillasComponent', () => {
  let component: GenerarPlanillasComponent;
  let fixture: ComponentFixture<GenerarPlanillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarPlanillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPlanillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
