import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransaccionesRoutingModule } from './transacciones-routing.module';
import { GenerarPlanillasComponent } from './generar-planillas/generar-planillas.component';
import { AsignarActividadesComponent } from './asignar-actividades/asignar-actividades.component';
import { ListaAsignacionActividadesComponent } from './lista-asignacion-actividades/lista-asignacion-actividades.component';
import { ReportesModule } from '../reportes/reportes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListaAsignacionActividadesLoteComponent } from './lista-asignacion-actividades-lote/lista-asignacion-actividades-lote.component';
import { AsignarActividadesLoteComponent } from './asignar-actividades-lote/asignar-actividades-lote.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PlanillaCultivosComponent } from './planilla-cultivos/planilla-cultivos.component';
import { PlanillaLoteActividadesComponent } from './planilla-lote-actividades/planilla-lote-actividades.component';
import { PlanillaEmpleadoActividadesComponent } from './planilla-empleado-actividades/planilla-empleado-actividades.component';
import { ResumenComponent } from './resumen/resumen.component';
import { GenerarResumenComponent } from './generar-resumen/generar-resumen.component';
import { ResumenEmpleadoActividadesComponent } from './resumen-empleado-actividades/resumen-empleado-actividades.component';
import { ResumenLoteActividadesComponent } from './resumen-lote-actividades/resumen-lote-actividades.component';
import { VerificarActividadesComponent } from './verificar-actividades/verificar-actividades.component';


@NgModule({
  declarations: [GenerarPlanillasComponent, AsignarActividadesComponent, ListaAsignacionActividadesComponent, ListaAsignacionActividadesLoteComponent, AsignarActividadesLoteComponent, PlanillaCultivosComponent, PlanillaLoteActividadesComponent, PlanillaEmpleadoActividadesComponent, ResumenComponent, GenerarResumenComponent, ResumenEmpleadoActividadesComponent, ResumenLoteActividadesComponent, VerificarActividadesComponent],
  imports: [
    CommonModule,
    TransaccionesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports:[
    PlanillaEmpleadoActividadesComponent, 
    PlanillaLoteActividadesComponent
  ],
  entryComponents:[
    AsignarActividadesComponent,
    AsignarActividadesLoteComponent
  ]
})
export class TransaccionesModule { }
