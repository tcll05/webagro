import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanillaCultivosComponent } from './planilla-cultivos.component';

describe('PlanillaCultivosComponent', () => {
  let component: PlanillaCultivosComponent;
  let fixture: ComponentFixture<PlanillaCultivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanillaCultivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanillaCultivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
