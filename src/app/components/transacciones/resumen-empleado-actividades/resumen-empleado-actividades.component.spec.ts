import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenEmpleadoActividadesComponent } from './resumen-empleado-actividades.component';

describe('ResumenEmpleadoActividadesComponent', () => {
  let component: ResumenEmpleadoActividadesComponent;
  let fixture: ComponentFixture<ResumenEmpleadoActividadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenEmpleadoActividadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenEmpleadoActividadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
