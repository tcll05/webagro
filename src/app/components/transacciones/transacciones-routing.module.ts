import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsignarActividadesComponent } from './asignar-actividades/asignar-actividades.component';
import { GenerarPlanillasComponent } from './generar-planillas/generar-planillas.component';
import { ListaAsignacionActividadesComponent } from './lista-asignacion-actividades/lista-asignacion-actividades.component';
import { ListaAsignacionActividadesLoteComponent } from './lista-asignacion-actividades-lote/lista-asignacion-actividades-lote.component';
import { AsignarActividadesLoteComponent } from './asignar-actividades-lote/asignar-actividades-lote.component';
import { PlanillaCultivosComponent } from './planilla-cultivos/planilla-cultivos.component';
import { PlanillaEmpleadoActividadesComponent } from './planilla-empleado-actividades/planilla-empleado-actividades.component';
import { PlanillaLoteActividadesComponent } from './planilla-lote-actividades/planilla-lote-actividades.component';
import { ResumenComponent } from './resumen/resumen.component';
import { GenerarResumenComponent } from './generar-resumen/generar-resumen.component';
import { ResumenEmpleadoActividadesComponent } from './resumen-empleado-actividades/resumen-empleado-actividades.component';
import { ResumenLoteActividadesComponent } from './resumen-lote-actividades/resumen-lote-actividades.component';
import { VerificarActividadesComponent } from './verificar-actividades/verificar-actividades.component';


const routes: Routes = [
  {
    path: 'actividadesLote',
    component: ListaAsignacionActividadesLoteComponent
  },
  {
    path: 'actividadesLote/crear',
    component: AsignarActividadesLoteComponent
  },
  {
    path: 'actividades',
    component: ListaAsignacionActividadesComponent
  },
  {
    path: 'actividades/crear',
    component: AsignarActividadesComponent
  },
  {
    path: 'planillas',
    component: GenerarPlanillasComponent
  },
  {
    path: 'resumen',
    component: GenerarResumenComponent
  },
  {
    path: 'resumen/cosechas',
    component: ResumenComponent
  },
  {
    path: 'resumen/actividad',
    component: ResumenEmpleadoActividadesComponent
  },
  {
    path: 'resumen/actividadLote',
    component: ResumenLoteActividadesComponent
  },
  {
    path: 'planillas/cosechas',
    component: PlanillaCultivosComponent
  },
  {
    path: 'planillas/actividad',
    component: PlanillaEmpleadoActividadesComponent
  },
  {
    path: 'planillas/actividadLote',
    component: PlanillaLoteActividadesComponent
  },
  {
    path: 'verificarActividades',
    component: VerificarActividadesComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransaccionesRoutingModule { }
