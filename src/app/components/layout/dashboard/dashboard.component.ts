import { Component, OnInit } from '@angular/core';
import moment from 'moment';
import { UtilsService } from 'src/app/Services/utils.service';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Cosechas } from 'src/app/models/Cosechas';
import Swal from "sweetalert2/dist/sweetalert2.js";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    inicioMes: string = "";
    fechaInicio: string;
    fechaFin: string;
    finMes: string = "";
    cosechas: Cosechas[] = []
    nActividades: number = 0;
    nCosechas: number = 0;
    nInversiones: number = 0;
    nplanillas: number = 0;

    totalActividades: number = 0;
    totalCosechas: number = 0;
    totalInversiones: number = 0;
    totalPlanillas: number = 0;
    constructor(private utils: UtilsService, private peticion: PeticionService) { }
    $table: any;
    ngOnInit(): void {

        this.getCosechas(true);
    }

    getDates() {
        this.fechaInicio = moment().subtract(15, 'days').format();
        this.fechaFin = moment().format();
        this.inicioMes = this.utils.dateFormatter(this.fechaInicio)
        this.finMes = this.utils.dateFormatter(this.fechaFin)
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

    getCosechas(inicio: boolean) {
        this.showLoading("Cargando datos...");
        if (inicio)
            this.getDates();
        let data = {
            fechaInicio: this.fechaInicio,
            fechaFinal: this.fechaFin,
            agrupar: true,
            pagada: false
        }
        console.log(data)
        this.peticion.getAll<any>(`dashboard/${data.fechaInicio}/${data.fechaFinal}/false`).subscribe(
            result => {
                Swal.close();
                this.cosechas = result.resumenCosechas;
                this.$table.bootstrapTable('load', this.cosechas)

                this.totalActividades = result.totalActividades;
                this.totalCosechas = result.totalCosechas;
                this.totalInversiones = result.totalInversiones;
                this.totalPlanillas = result.totalPlanillas;

                this.nActividades = result.actividades
                this.nCosechas = result.cosechas
                this.nInversiones = result.inversiones
                this.nplanillas = result.planillas
                Swal.close();
            },
            error => {
                Swal.close();
            }
        )
    }

    buscarFecha() {
        this.fechaFin = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
        this.inicioMes = this.utils.dateFormatter(this.fechaInicio)
        this.finMes = this.utils.dateFormatter(this.fechaFin)
        this.getCosechas(false);
    }

    ngAfterViewInit() {
        var self = this;

        (<any>$('#reservation')).daterangepicker();

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.pagada) {
                badges = `
      <span class="badge badge-danger">Sin pagar</span>
      `;

            }
            if (row.pagada) {
                badges = `<span class="badge badge-secondary">Pagada</span>`
            }
            return [
                badges
            ].join('')
        };

        function totalCantidadFormatter(data) {
            var field = this.field
            return data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function totalPagoFormatter(data) {
            var field = this.field
            return 'L. ' + data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)
        }

        function priceFormatter(value, row, index) {
            return "L. " + value;
        };

        function dateFormatter(value, row, index) {
            return self.utils.dateTimeFormatter(value);
        };
        self.$table = $('#table');
        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable('destroy').bootstrapTable({
            data: self.cosechas,
            pagination: true,
            search: true,
            showFooter: true,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            columns: [
                {
                    field: 'empleadoCodigo',
                    sortable: true,
                    title: 'Codigo'
                },
                {
                    field: 'empleadoNombre',
                    sortable: true,
                    title: 'Empleado'
                },
                {
                    field: 'cantidad',
                    sortable: true,
                    title: 'Cantidad',
                    footerFormatter: totalCantidadFormatter
                },
                {
                    field: 'totalPago',
                    sortable: true,
                    formatter: priceFormatter,
                    footerFormatter: totalPagoFormatter,
                    title: 'Pago'
                },
                {
                    field: 'fechaCreacion',
                    sortable: true,
                    formatter: dateFormatter,
                    title: 'Fecha'
                }
            ]
        });
    }

}
