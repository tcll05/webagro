import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemaRoutingModule } from './tema-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [SidebarComponent, DashboardComponent],
  imports: [
    CommonModule,
    TemaRoutingModule
  ],
  exports:[
    SidebarComponent
  ]
})
export class TemaModule { }
