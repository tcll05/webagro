import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  isLoggedIn$: Observable<boolean>; 
  user:string=""
  constructor(private router: Router, private sesion:AuthService) { }

  ngOnInit(): void {
    $('[data-widget="treeview"]').Treeview('init');
    this.isLoggedIn$ = this.sesion.isLoggedIn;
    this.user=sessionStorage.getItem("user");
  }

  descargarAplicacion(){
    
  }

  cerrarSesion()
  {
    this.sesion.logout();
    this.router.navigate(['/acceso'])
  }

}
