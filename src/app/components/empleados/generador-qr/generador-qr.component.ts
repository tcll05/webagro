import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas'

import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-generador-qr',
    templateUrl: './generador-qr.component.html',
    styleUrls: ['./generador-qr.component.css']
})
export class GeneradorQRComponent implements OnInit {
    @Input()
    empleados: any = [];
    @ViewChild('htmlData') htmlData: ElementRef;
    @Output() view: EventEmitter<boolean> = new EventEmitter();
    name = environment.name;
    constructor() { }

    ngOnInit(): void {
    }

    print() {
        window.print();
    }

    DownloadFromHTML() {
        let DATA = document.getElementById("htmlData")


        let totalHeight = DATA.offsetHeight;
        let doc = new jsPDF('p', 'pt', 'letter');
        let doc2 = new jsPDF('p', 'pt', 'a4');
        //const pdf = new jsPDF(doc.orientation, doc.unit, doc.format);
        const pdfWidth = doc.internal.pageSize.width;
        const pdfHeight = doc.internal.pageSize.height;
        window.scrollTo(0, 0);
        html2canvas(DATA, {
            'allowTaint': false,
            'backgroundColor': '#ffffff',
            'canvas': null,
            'foreignObjectRendering': false,
            'imageTimeout': 15000,
            'logging': false,
            'onclone': null,
            'proxy': null,
            'removeContainer': true,
            'scale': window.devicePixelRatio,
            'useCORS': false,
        }).then((canvas) => {
            const widthRatio = pdfWidth / canvas.width;
            const sX = 0;
            const sWidth = canvas.width;
            const sHeight = pdfHeight + ((pdfHeight - pdfHeight * widthRatio) / widthRatio);
            const dX = 0;
            const dY = 0;
            const dWidth = sWidth;
            const dHeight = sHeight;
            let pageCnt = 1;
            while (totalHeight > 0) {
                totalHeight -= sHeight;
                let sY = sHeight * (pageCnt - 1);
                const childCanvas: any = document.createElement('CANVAS');
                childCanvas.setAttribute('width', sWidth + "");
                childCanvas.setAttribute('height', sHeight + "");
                const childCanvasCtx = childCanvas.getContext('2d');
                childCanvasCtx.drawImage(canvas, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

                if (pageCnt > 1) {
                    doc2.addPage();
                }
                doc2.setPage(pageCnt);
                doc2.addImage(childCanvas.toDataURL('image/png'), 'PNG', 0, 22, canvas.width * widthRatio, 0);
                pageCnt++;
            }
            /* for(var i = 1; i <= pageCnt; i++) {
                doc2.setPage(i)
                // HEADER
                doc2.setFontSize(20);//optional
                doc2.setTextColor(40);//optional
                doc2.setFontStyle('normal');//optional
                doc2.text("Report", 50, 22);// set your margins
            } */

            doc2.save("carnets.pdf");
        });
        window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }
    regresar() {
        this.view.emit(true);
    }
    getData(empleado) {
        return JSON.stringify(empleado);
        //return CryptoJS.AES.encrypt("@mendozadev-1erw-kr7e-2hy", data.trim()).toString();  
    }

}
