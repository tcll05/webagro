import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { EmpleadosRoutingModule } from './empleados-routing.module';
import { ListaEmpleadosComponent } from './lista-empleados/lista-empleados.component';
import { CrearEmpleadosComponent } from './crear-empleados/crear-empleados.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GeneradorQRComponent } from './generador-qr/generador-qr.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { CrearUsuariosComponent } from './crear-usuarios/crear-usuarios.component';
import { HistorialComponent } from './historial/historial.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CambioContrasenaComponent } from './cambio-contrasena/cambio-contrasena.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [ListaEmpleadosComponent, CrearEmpleadosComponent, GeneradorQRComponent, ListaUsuariosComponent, CrearUsuariosComponent, HistorialComponent, CambioContrasenaComponent],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    NgbDatepickerModule,
    QRCodeModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxMaskModule
  ],
  entryComponents:[
    CrearEmpleadosComponent
  ]
})
export class EmpleadosModule { }
