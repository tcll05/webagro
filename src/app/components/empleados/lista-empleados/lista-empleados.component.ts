import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrearEmpleadosComponent } from '../crear-empleados/crear-empleados.component';
import { GeneradorQRComponent } from '../generador-qr/generador-qr.component';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Empleado } from 'src/app/models/Empleados';
import { UtilsService } from 'src/app/Services/utils.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { VacacionesComponent } from 'src/app/shared/vacaciones/vacaciones.component';
import { AlertService } from 'src/app/Services/alert.service';

@Component({
    selector: 'app-lista-empleados',
    templateUrl: './lista-empleados.component.html',
    styleUrls: ['./lista-empleados.component.css']
})
export class ListaEmpleadosComponent implements OnInit {

    imprimir: boolean = false;
    seleccion: Empleado[] = [];
    empleados = [];
    saveEmpleado: Empleado
    mode: string = 'table';
    @ViewChild('QR', { static: false }) generarQR: GeneradorQRComponent;
    constructor(private alert:AlertService, private modalService: NgbModal, private peticion: PeticionService, private utils: UtilsService) { }

    ngOnInit(): void {

    }


    ngAfterViewInit() {
        this.createTable();
    }

    createTable() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            let btn: string = "";
            btn+=`<div class="btn-group" role="group">`;
            btn += `<button class="btn btn-primary btn-sm edit"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`;
            if (row.activo) {
                btn += `<button class="btn btn-outline-info btn-sm delete">Desactivar</button>`;
            }
            else {
                btn += `<button class="btn btn-outline-info btn-sm delete">Activar</button>`;
            }
            if (row.deVacacion) {
                btn += `<button class="btn btn-outline-info btn-sm vacaciones">Reintegrar</button>`;
            }
            else {
                btn += `<button class="btn btn-outline-info btn-sm vacaciones">Vacaciones</button>`;
            }
            btn+="</div>"
            return [
                btn

            ].join('')
        };

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.activo) {
                badges = `
              <span class="badge badge-danger">Inactivo</span>
              `;

            }
            if (row.activo) {
                badges = `<span class="badge badge-success">Activo</span>`
            }
            if (row.deVacacion) {
                badges = `
              <span class="badge badge-danger">De vacaciones</span>
              `;

            }
            return [
                badges
            ].join('')
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                if(row.activo)
                self.desactivar(row, false);//alert('You click like action, row: ' + JSON.stringify(row))
                else
                self.desactivar(row, true);
            },
            'click .vacaciones': function (e, value, row, index) {
                if(row.deVacacion)
                self.openFecha(row, false);//alert('You click like action, row: ' + JSON.stringify(row))
                else
                self.openFecha(row, true);
            }
        };
        function dateFormatter(value, row, index) {
            return self.utils.dateFormatter(value);
        };

        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            url: `${url}/empleado`,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            pagination: true,
            search: true,
            queryParams: queryParams,
            showRefresh: true,
            size: 20,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            exportDataType:"all",          
            columns: [
                {
                    field: 'state',
                    checkbox: true,
                    align: 'center',
                    valign: 'middle'
                },
                {
                    field: 'codigo',
                    title: 'Codigo'
                },
                {
                    field: 'documento',
                    title: 'Documento'
                },
                {
                    field: 'nombres',
                    
                    title: 'Nombres'
                },
                {
                    field: 'apellidos',
                    title: 'Apellidos'
                },
                {
                    field: 'cargoNombre',
                    title: 'Cargo'
                },
                {
                    field: 'sexo',
                    title: 'Sexo'
                },
                {
                    field: 'activo',
                    formatter: "estados",
                    title: 'Estado'
                },
                {
                    field: 'fechaNacimiento',
                    formatter: dateFormatter,
                    title: 'Fecha de nacimiento'
                },
                {
                    field: 'fechaIngreso',
                    formatter: dateFormatter,
                    title: 'Fecha de ingreso'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    formatter: "acciones",
                    forceHide:true,
                    events: "operateEvents"
                }
            ]

        });
        let $table: any = $('#table');

        $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table',
            function () {
                self.selected($table.bootstrapTable('getSelections').length > 0);
                self.seleccion = getIdSelections();

                // push or splice the selections if you want to save all data selections
            })

        function getIdSelections() {
            return $.map($table.bootstrapTable('getSelections'), function (row) {
                return row;
            })
        }
    }

    openFecha(empleado:Empleado, estado:boolean) {
        let mensaje=""; if(!estado) mensaje="reintegrar"; else mensaje = "enviar de vacaciones";
        
        const modalRef = this.modalService.open(VacacionesComponent, { scrollable: false, centered: true });
        modalRef.componentInstance.nombre = `${empleado.nombres} ${empleado.apellidos}`;
        modalRef.componentInstance.accion = mensaje;
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                let tmpDate: string = `${receivedEntry.year}-${receivedEntry.month}-${receivedEntry.day}`;
                //console.log(tmpDate)
                this.peticion.post<any>(`empleado/historial/${empleado.empleadoId}/${tmpDate}`, undefined).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro completo!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                result.mensaje,
                                'error'
                            );
                                (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        console.log(error)
                        this.alert.mensaje(
                            'Error',
                            error.error.mensaje,
                            'error'
                        )
                    }
                )
            })
    }

    nuevo() {
        const modalRef = this.modalService.open(CrearEmpleadosComponent, { scrollable: true, size: 'lg' });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveEmpleado = receivedEntry;
                //formateamos la fecha yyyy/MM/dd
                this.saveEmpleado.cargoId = +this.saveEmpleado.cargoId;
                this.saveEmpleado.fechaNacimiento = new Date(`${receivedEntry.fechaNacimiento.year}/${receivedEntry.fechaNacimiento.month}/${receivedEntry.fechaNacimiento.day}`);
                this.saveEmpleado.activo = true;
                //hacemos el post mediante el servicio de peticion
                this.peticion.post<any>("empleado/", this.saveEmpleado).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )
            })
    }

    editar(persona: Empleado) {
        const modalRef = this.modalService.open(CrearEmpleadosComponent, { scrollable: true, size: 'lg' });
        //enviamos la persona a modificar al componente de crear-persona
        modalRef.componentInstance.tmpEmpleado = persona;

        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveEmpleado = receivedEntry;
                this.saveEmpleado.activo = true;
                this.saveEmpleado.empleadoId = +this.saveEmpleado.empleadoId;
                this.saveEmpleado.cargoId = +this.saveEmpleado.cargoId;
                this.saveEmpleado.fechaNacimiento = new Date(`${receivedEntry.fechaNacimiento.year}/${receivedEntry.fechaNacimiento.month}/${receivedEntry.fechaNacimiento.day}`);
                //enviamos mediante el metodo put el registro a modificar
                this.peticion.put<any>("empleado", this.saveEmpleado, receivedEntry.empleadoId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue modificado...',
                                'error'
                            );
                                (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al modificar el registro!',
                            'error'
                        )
                    }
                )

                //this.data.push(this.savePersona);

            })
    }

    desactivar(empleado: Empleado, estado:boolean) {
        let mensaje=""; if(estado) mensaje="activar"; else mensaje = "desactivar";
        Swal.fire({
            title: 'Modificar Registro',
            text: `Seguro desea ${mensaje} el empleado ${empleado.nombres} ${empleado.apellidos}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {

            if (result.value) {
                empleado.activo = estado;
                this.peticion.post<any>(`empleado/${empleado.empleadoId}/${estado}`, undefined).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                result.mensaje,
                                'error'
                            );
                                (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            result.error.mensaje,
                            'error'
                        )
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });
    }

    verTabla(event: boolean) {
        location.reload();
        this.imprimir = false;
        this.seleccion = null
        //this.createTable();

    }

    imprimirQR() {
        this.seleccion.forEach(element => {
            console.log(element);
            this.empleados.push(
                {
                    id: element.empleadoId,
                    codigo: element.codigo,
                    documento: element.documento,
                    nombres: element.nombres,
                    apellidos: element.apellidos
                }
            )
            console.log(this.empleados)
        });
        this.mode = 'QR';

    }

    selected(state: boolean) {
        this.imprimir = state;
    }

    getRow(row) {
        console.log(row)
    }
}
