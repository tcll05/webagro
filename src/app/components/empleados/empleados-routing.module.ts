import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaEmpleadosComponent } from './lista-empleados/lista-empleados.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { HistorialComponent } from './historial/historial.component';
import { CambioContrasenaComponent } from './cambio-contrasena/cambio-contrasena.component';


const routes: Routes = [
  {
    path:'',
    component:ListaEmpleadosComponent
  },
  {
    path:'usuarios',
    component:ListaUsuariosComponent
  },
  {
    path:'historial',
    component:HistorialComponent
  },
  {
    path:'cambio-contrasenia',
    component:CambioContrasenaComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
