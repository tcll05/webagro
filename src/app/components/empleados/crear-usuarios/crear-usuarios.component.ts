import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuarios } from 'src/app/models/Catalogos';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';

@Component({
  selector: 'app-crear-usuarios',
  templateUrl: './crear-usuarios.component.html',
  styleUrls: ['./crear-usuarios.component.css']
})
export class CrearUsuariosComponent implements OnInit {

  formUsuario: FormGroup;
  Usuario: Usuarios;
  //Variable de entrada para modificar una Usuario registrada
  @Input() public tmpUsuario: Usuarios;
  //variable de salida para retornar la Usuario que se registro
  @Output() passEntry: EventEmitter<Usuarios> = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private peticion: PeticionService) { }

  ngOnInit(): void {
    this.formUsuario = this.formBuilder.group({
      nombre: [null, [Validators.required, Validators.maxLength(13)]],
      contrasenia: [null, [Validators.required, Validators.maxLength(50)]],
      contrasenia2: [null, [Validators.required, Validators.maxLength(120)]],
      accesoMovil: [false, [Validators.required, Validators.maxLength(120)]],
      accesoWeb: [false, [Validators.required, Validators.maxLength(120)]],
      activo: [true, [Validators.required, Validators.maxLength(120)]],
    })
    // si se esta modificando una Usuario, se formatea la fecha para poder ser visualizada
    if (this.tmpUsuario != null) {
      this.formUsuario.patchValue(this.tmpUsuario);
    }
  }

  guardar() {
    /*si el formulario es invalido (si no cumple con todas las validaciones)
    * muestra los mensajes de validacion         
    */
    if (this.formUsuario.invalid) {
      this.formUsuario.markAllAsTouched();
      return;
    }
    else {
      // al modelo Usuario le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
      this.Usuario = this.formUsuario.value;

      //si la variable de entrada tmpUsuario (modificar) no es nula, 
      //le asigna el id al objeto Usuario que sera registrado
      if (this.tmpUsuario != null)
        this.Usuario.usuarioId = this.tmpUsuario.usuarioId;
      // retornamos a la pantalla de listado el objeto Usuario con todos los datos que fueron ingresados
      this.passEntry.emit(this.Usuario);
      //se vacian los campos y se cierra la modal
      this.formUsuario.reset();
      this.activeModal.close();

    }

  }

  validarContrasenia() {
    let contrasenia1 = this.formUsuario.get('contrasenia').value;
    let contrasenia2 = this.formUsuario.get('contrasenia2').value;

    if (contrasenia1 == contrasenia2)
      return true;
    else return false;
  }

}
