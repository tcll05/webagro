import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cambio-contrasena',
  templateUrl: './cambio-contrasena.component.html',
  styleUrls: ['./cambio-contrasena.component.css']
})
export class CambioContrasenaComponent implements OnInit {

  formInicio: FormGroup;
  usuario:string="";
  constructor(private builder: FormBuilder) { }

  ngOnInit(): void {
    this.usuario=sessionStorage.getItem("user");
    this.formInicio = this.builder.group({
      contraseniaActual: ['', [Validators.required]],
      contrasenia: ['', [Validators.required]],
      contrasenia2: ['', [Validators.required]]
    })
  }

  cambiar() {

  }

  validarContrasenia(){
    if(this.formInicio.get("contrasenia").value == this.formInicio.get("contrasenia2").value)
    return true;
    return false;
  }

}
