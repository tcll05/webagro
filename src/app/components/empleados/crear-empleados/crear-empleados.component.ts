import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Empleado } from 'src/app/models/Empleados';
import { Cargos } from 'src/app/models/Catalogos';
import { PeticionService } from 'src/app/Services/peticion.service';
import { paises } from 'src/app/models/Paises';

//import swal from ''
@Component({
    selector: 'app-crear-empleados',
    templateUrl: './crear-empleados.component.html',
    styleUrls: ['./crear-empleados.component.css']
})
export class CrearEmpleadosComponent implements OnInit {
    formEmpleado: FormGroup;
    Empleado: Empleado;
    cargos: Cargos[] = [];
    paises = paises;
    //Variable de entrada para modificar una Empleado registrada
    @Input() public tmpEmpleado: Empleado;
    //variable de salida para retornar la Empleado que se registro
    @Output() passEntry: EventEmitter<Empleado> = new EventEmitter();
    pattern = {'0': { pattern: new RegExp('\[a-zA-Z_ \]')}};
    constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private peticion: PeticionService) { }

    ngOnInit(): void {
        this.formEmpleado = this.formBuilder.group({
            documento: [null, [Validators.required, Validators.maxLength(13)]],
            codigo: [null, [Validators.required, Validators.maxLength(50)]],
            nombres: [null, [Validators.required, Validators.maxLength(120)]],
            apellidos: [null, [Validators.required, Validators.maxLength(120)]],
            fechaNacimiento: [Date, [Validators.required]],
            fechaInicio: [Date, [Validators.required]],
            nacionalidad: [null, [Validators.required]],
            sexo: [null, [Validators.required, Validators.maxLength(1)]],
            telefono: [null, [Validators.maxLength(8), Validators.minLength(8)]],
            correoElectronico: [null, Validators.maxLength(120)],
            cargoId: [0, [Validators.required]],
        })
        
        let tmpDateX = {
            "year": 1900,
            "month": 1,
            "day": 1
        };
        this.formEmpleado.patchValue({ "fechaNacimiento": tmpDateX })

        this.getCargos();
        // si se esta modificando una Empleado, se formatea la fecha para poder ser visualizada
        if (this.tmpEmpleado != null) {
            let fechaTemp = new Date(this.tmpEmpleado.fechaNacimiento)
            let tmpDate = {
                "year": fechaTemp.getFullYear(),
                "month": fechaTemp.getMonth() + 1,
                "day": fechaTemp.getDate()
            };
            //al formulario se le asignan los datos recibidos en tmpEmpleado
            this.formEmpleado.patchValue(this.tmpEmpleado);
            this.formEmpleado.patchValue({ "fechaNacimiento": tmpDate })
        }

    }

    getCargos() {
        this.peticion.getAll<Cargos[]>("cargo").subscribe(
            result => {
                this.cargos = result;
            }
        )
    }

    guardar() {
        /*si el formulario es invalido (si no cumple con todas las validaciones)
        * muestra los mensajes de validacion         
        */
       debugger
        console.log(this.formEmpleado)
        if (this.formEmpleado.invalid) {
            this.formEmpleado.markAllAsTouched();
            return;
        }
        else if (this.validarGenero()) {
            this.formEmpleado.markAllAsTouched();
            return;
        }
        else {
            // al modelo Empleado le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
            this.Empleado = this.formEmpleado.value;

            //si la variable de entrada tmpEmpleado (modificar) no es nula, 
            //le asigna el id al objeto Empleado que sera registrado
            if (this.tmpEmpleado != null)
                this.Empleado.empleadoId = this.tmpEmpleado.empleadoId;
            // retornamos a la pantalla de listado el objeto Empleado con todos los datos que fueron ingresados
            this.passEntry.emit(this.Empleado);
            //se vacian los campos y se cierra la modal
            this.formEmpleado.reset();
            this.activeModal.close();

        }

    }
    //valida que el genero seleccionado sea correcto
    validarGenero() {
        if (this.formEmpleado.get('sexo').value == "F" || this.formEmpleado.get('sexo').value == "M")
            return false;
        else return true;
    }

    validarCargo() {
        if (this.formEmpleado.get('cargoId').value == 0)
            return true;
        else return false;
    }

}
