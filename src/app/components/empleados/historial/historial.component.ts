import { Component, OnInit } from '@angular/core';
import { PeticionService } from 'src/app/Services/peticion.service';
import { Fincas, Lotes, Cultivos, Planilla } from 'src/app/models/Catalogos';
import { Empleado } from 'src/app/models/Empleados';
import moment from 'moment';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { UtilsService } from 'src/app/Services/utils.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuscarEmpleadosComponent } from 'src/app/shared/buscar-empleados/buscar-empleados.component';

@Component({
    selector: 'app-historial',
    templateUrl: './historial.component.html',
    styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

    empleado: string = "";
    empleadosSelected: number = 0;
    fechaInicio: string;
    fechaFinal: string;
    empleadoId:number=0;
    historial:any;
    $table: any;
    constructor(private peticion: PeticionService, private modalService: NgbModal, private router: Router, private utils: UtilsService) { }

    ngOnInit(): void {
        //this.getData();
    }

    getData() {

        this.showLoading("Cargando datos...")
        this.fechaFinal = moment($('#reservation').data('daterangepicker').endDate).format("YYYY-MM-DD");
        this.fechaInicio = moment($('#reservation').data('daterangepicker').startDate).format("YYYY-MM-DD");
        
        this.peticion.getAll<any>(`empleado/historial/${this.fechaInicio}/${this.fechaFinal}/${this.empleadoId}`).subscribe(
            result => {
                Swal.close();
                this.historial = result;
                this.$table.bootstrapTable('load', this.historial)
            },
            error => {
                Swal.close();

            }
        )


    }

    buscarEmpleado() {
        const modalRef = this.modalService.open(BuscarEmpleadosComponent, { scrollable: true, size: 'lg' });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                console.log(receivedEntry)
                this.empleado = `${receivedEntry.nombres} ${receivedEntry.apellidos}`;
                this.empleadoId=receivedEntry.empleadoId;
                //this.nombreEmpleado=`${receivedEntry.nombres} ${receivedEntry.apellidos}`;
            })
    }

    showLoading(title) {
        Swal.fire({
            title: title,
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
    }

    ngAfterViewInit() {
        var self = this;
        /*(<any>$('.select2bs4')).select2({
          theme: 'bootstrap4'
        });*/
        (<any>$('#reservation')).daterangepicker(function (start, end, label) {
            self.fechaInicio = start;
            self.fechaFinal = end;
        });

        (<any>window).acciones = function (value, row, index) {
            return [
                `<button class="btn btn-block btn-primary btn-sm edit mr-2">Ver detalles</button>`
            ].join('')
        };

        function datetimeFormatter(value, row, index) {
            return self.utils.dateFormatter(value)
        };

        function nombreFormatter(value, row, index) {
            return `${row.nombres} ${row.apellidos}`;
        };

        self.$table = $('#table');

        (<any>$('#table')).bootstrapTable({
            data: self.historial,
            pagination: true,
            sortName: 'codigo',
            sortOrder: 'asc',
            search: true,
            size: 100,
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
            locale: 'es-CR',
            showExport: true,
            columns: [
                {
                    field: 'codigo',
                    sortable: true,
                    title: 'Codigo'
                },
                {
                    field: 'nombres',
                    title: 'Empleado',
                    formatter: nombreFormatter,
                    sortable: true,
                },
                {
                    field: 'fechaVacacion',
                    formatter: datetimeFormatter,
                    title: 'Fecha de salida',
                    sortable: true
                },
                {
                    field: 'fechaReingreso',
                    formatter: datetimeFormatter,
                    title: 'Fecha de reingreso',
                    sortable: true
                }
            ]
        });
    }
}
