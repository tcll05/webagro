import { Component, OnInit, ViewChild } from '@angular/core';
import { CrearUsuariosComponent } from '../crear-usuarios/crear-usuarios.component';
import { Usuarios } from 'src/app/models/Catalogos';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeticionService } from 'src/app/Services/peticion.service';
import { UtilsService } from 'src/app/Services/utils.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AlertService } from 'src/app/Services/alert.service';
@Component({
    selector: 'app-lista-usuarios',
    templateUrl: './lista-usuarios.component.html',
    styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

    usuarios = [];
    saveUsuario: Usuarios
    mode: string = 'table';
    constructor(private alert: AlertService, private modalService: NgbModal, private peticion: PeticionService, private utils: UtilsService) { }

    ngOnInit(): void {

    }


    ngAfterViewInit() {
        this.createTable();
    }

    createTable() {
        var self = this;
        (<any>window).acciones = function (value, row, index) {
            let btn: string = "";
            btn += `<button class="btn btn-primary btn-sm edit mr-2"><i class="fas fa-pencil-alt"></i>&nbsp;Editar</i></button>`;
            if (row.activo) {
                btn += `<button class="btn btn-outline-info btn-sm delete mr-2">Desactivar</button>`;
            }
            else {
                btn += `<button class="btn btn-outline-info btn-sm delete mr-2">Activar</button>`;
            }
            return [
                btn

            ].join('')
        };

        (<any>window).estados = function (value, row, index) {
            let badges = "";
            if (!row.activo) {
                badges = `
              <span class="badge badge-danger">Inactivo</span>
              `;

            }
            if (row.activo) {
                badges = `<span class="badge badge-success">Activo</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).accesos = function (value, row, index) {
            let badges = "";
            if (row.accesoMovil) {
                badges = `
              <span class="badge badge-success">Si</span>
              `;

            }
            if (!row.accesoMovil) {
                badges = `<span class="badge badge-danger">No</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).accesosWeb = function (value, row, index) {
            let badges = "";
            if (row.accesoWeb) {
                badges = `
                <span class="badge badge-success">Si</span>
                `;

            }
            if (!row.accesoWeb) {
                badges = `<span class="badge badge-danger">No</span>`
            }
            return [
                badges
            ].join('')
        };

        (<any>window).operateEvents = {
            'click .edit': function (e, value, row, index) {
                self.editar(row);//alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .delete': function (e, value, row, index) {
                if (row.activo)
                    self.desactivar(row, false);//alert('You click like action, row: ' + JSON.stringify(row))
                else
                    self.desactivar(row, true)
            }
        };
        function dateFormatter(value, row, index) {
            return self.utils.dateFormatter(value);
        };

        function queryParams() {
            return {
            };
        }

        let url = this.peticion.getBaseURL();
        (<any>$('#table')).bootstrapTable({
            url: `${url}/usuario`,
            ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
            pagination: true,
            search: true,
            queryParams: queryParams,
            showRefresh: true,
            size: 20,
            locale: 'es-CR',
            columns: [
                {
                    field: 'nombre',
                    title: 'Usuario'
                },
                {
                    field: 'activo',
                    formatter: "estados",
                    title: 'Estado'
                },

                {
                    field: 'accesoMovil',
                    formatter: "accesos",
                    title: 'Acceso movil'
                },
                {
                    field: 'accesoWeb',
                    formatter: "accesosWeb",
                    title: 'Acceso web'
                },
                {
                    field: 'acciones',
                    title: 'Acciones',
                    width: 195,
                    widthUnit: "px",
                    formatter: "acciones",
                    events: "operateEvents"
                }
            ]

        });
    }

    desactivar(usuario: Usuarios, estado: boolean) {
        let mensaje = ""; if (estado) mensaje = "activar"; else mensaje = "desactivar";
        Swal.fire({
            title: 'Modificar Registro',
            text: `Seguro desea ${mensaje} el usuario ${usuario.nombre}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {

            if (result.value) {
                usuario.activo = estado;
                this.peticion.post<any>(`usuario/${usuario.usuarioId}/${estado}`, undefined).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue modificado...',
                                'error'
                            );
                            (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al modificar el registro!',
                            'error'
                        )
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });
    }

    nuevo() {
        const modalRef = this.modalService.open(CrearUsuariosComponent, { scrollable: true, size: 'lg' });
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveUsuario = receivedEntry;
                //hacemos el post mediante el servicio de peticion
                this.peticion.post<any>("usuario/", this.saveUsuario).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.alert.mensaje(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        this.alert.mensaje(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )
            })
    }

    editar(persona: Usuarios) {
        const modalRef = this.modalService.open(CrearUsuariosComponent, { scrollable: true, size: 'lg' });
        //enviamos la persona a modificar al componente de crear-persona
        modalRef.componentInstance.tmpUsuario = persona;

        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.saveUsuario = receivedEntry;
                this.saveUsuario.usuarioId = +this.saveUsuario.usuarioId;
                this.peticion.put<any>("usuario", this.saveUsuario, receivedEntry.usuarioId).subscribe(
                    result => {
                        if (result.resultado == true) {
                            this.alert.mensaje(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            );
                            (<any>$('#table')).bootstrapTable('refresh');
                        }
                        else {
                            this.alert.mensaje(
                                'Error',
                                'El registro no fue modificado...',
                                'error'
                            );
                            (<any>$('#table')).bootstrapTable('refresh')
                        }
                    },
                    error => {
                        this.alert.mensaje(
                            'Error',
                            'Error al modificar el registro!',
                            'error'
                        )
                    }
                )

                //this.data.push(this.savePersona);

            })
    }

    getRow(row) {
        console.log(row)
    }

}
