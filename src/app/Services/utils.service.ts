import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { CurrencyPipe } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    monedas: number[] = [500, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.20, 0.10, 0.05];
    total: {
        cantidad: number,
        moneda: number
    }[] = [];

    constructor(private currencyPipe: CurrencyPipe,) { }

    dateFormatter(value) {

        moment.locale('es');
        return moment(value).format('DD/MM/YYYY');
    };

    timeFormatter(value) {
        moment.locale('es');
        return moment(value).format('h:mm a');
    }

    dateTimeFormatter(value) {
        moment.locale('es');
        return moment(value).format('DD/MM/YYYY h:mm a');
    }

    formatMoney(value) {
        const temp = `${value}`.replace(/\,/g, "");
        return this.currencyPipe.transform(temp).replace("$", "L. ");
    }

    getImporte(importe) {
        let rs = ""
        let cambio: number[] = []
        // Recorremos todas las monedas
        for (var i = 0; i < this.monedas.length; i++) {
            console.log("importe", importe);
            // Si el importe actual, es superior a la moneda
            if (importe >= this.monedas[i]) {

                // obtenemos cantidad de monedas
                cambio.push(parseInt((importe / this.monedas[i]).toString()));

                // actualizamos el valor del importe que nos queda por didivir
                importe = (importe - (cambio[cambio.length - 1] * this.monedas[i])).toFixed(2);
                rs += cambio[cambio.length - 1] + " billetes de: " + this.formatMoney(this.monedas[i]) + "</br>";
                console.log("importe2", importe);
            }
        }
        return rs;
    }

    getArrayImporte(importe) {
        this.total=[];
        let rs = ""
        let cambio: number[] = []
        // Recorremos todas las monedas
        for (var i = 0; i < this.monedas.length; i++) {
            // Si el importe actual, es superior a la moneda
            if (importe >= this.monedas[i]) {

                // obtenemos cantidad de monedas
                cambio.push(parseInt((importe / this.monedas[i]).toString()));

                // actualizamos el valor del importe que nos queda por didivir
                importe = (importe - (cambio[cambio.length - 1] * this.monedas[i])).toFixed(2);
                
                this.total.push({cantidad: cambio[cambio.length - 1], moneda: this.monedas[i]});
            }
        }
        return this.total;
    }
}
