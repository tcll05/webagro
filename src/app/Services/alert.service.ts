import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  loading(title) {
    Swal.fire({
      title: title,
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
  }

  mensaje(title, message, icon){
    Swal.fire({
      icon: icon,
      title: title,
      text: message,
      allowOutsideClick: false,
      confirmButtonText: `Aceptar`,
    })
  }
}
