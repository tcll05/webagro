import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-vacaciones',
  templateUrl: './vacaciones.component.html',
  styleUrls: ['./vacaciones.component.css']
})
export class VacacionesComponent implements OnInit {

  @Input() accion: string;
  @Input() nombre: string;
  @Output() passEntry: EventEmitter<string> = new EventEmitter();
  fecha:string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  guardar(){
    if(!isNullOrUndefined( this.fecha)){
      this.passEntry.emit(this.fecha);
      this.activeModal.close();
    }
    
  }

}
