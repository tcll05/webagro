import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapaComponent } from './mapa/mapa.component';
import { OlMapsModule } from './ol-maps/ol-maps.module';
import { BuscarEmpleadosComponent } from './buscar-empleados/buscar-empleados.component';
import { VacacionesComponent } from './vacaciones/vacaciones.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [MapaComponent, BuscarEmpleadosComponent, VacacionesComponent],
  imports: [
    CommonModule,
    OlMapsModule,
    NgbDatepickerModule,
    FormsModule
  ],
  exports:[
    MapaComponent, OlMapsModule
  ],
  entryComponents:[
    BuscarEmpleadosComponent,
    VacacionesComponent
  ]
})
export class SharedModule { }
