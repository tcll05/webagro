import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Empleado } from 'src/app/models/Empleados';
import { PeticionService } from 'src/app/Services/peticion.service';

@Component({
  selector: 'app-buscar-empleados',
  templateUrl: './buscar-empleados.component.html',
  styleUrls: ['./buscar-empleados.component.css']
})
export class BuscarEmpleadosComponent implements OnInit {

  
  @Output() passEntry: EventEmitter<Empleado> = new EventEmitter();
  constructor(public activeModal: NgbActiveModal, private peticion: PeticionService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    var self = this;
    (<any>window).acciones = function (value, row, index) {
        return [
            `<button class="btn btn-primary btn-sm seleccionar">Seleccionar</i></button>`
        ].join('')
    };

    (<any>window).operateEvents = {
        'click .seleccionar': function (e, value, row, index) {
            self.seleccionar(row);//alert('You click like action, row: ' + JSON.stringify(row))
        }
    };

    function queryParams() {
        return {
        };
    }

    let url = this.peticion.getBaseURL();
    (<any>$('#table1')).bootstrapTable({
        url: `${url}/empleado`,
        ajaxOptions: { headers: { 'Authorization': 'bearer ' + sessionStorage.getItem("isLoggedIn") } },
        pagination: true,
        search: true,
        queryParams: queryParams,
        showRefresh: true,
        size: 20,
        locale: 'es-CR',
        columns: [
            {
                field: 'codigo',
                title: 'Codigo'
            },
            {
                field: 'documento',
                title: 'Documento'
            },
            {
                field: 'nombres',
                title: 'Nombres'
            },
            {
                field: 'apellidos',
                title: 'Apellidos'
            },
            {
                field: 'cargoNombre',
                title: 'Cargo'
            },
            {
                field: 'sexo',
                title: 'Sexo'
            },
            {
                field: 'acciones',
                title: 'Acciones',
                formatter: "acciones",
                events: "operateEvents"
            }
        ]

    });
    let $table: any = $('#table1');
}

  seleccionar(empleado: any){
    this.passEntry.emit(empleado);
    this.activeModal.close();
  }

}
