import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './Services/auth.guard';


const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./components/layout/tema.module").then(m => m.TemaModule),
      canActivate:[AuthGuard]
  },
  {
    path: "empleados",
    loadChildren: () =>
      import("./Components/empleados/empleados.module").then(m => m.EmpleadosModule),
      canActivate:[AuthGuard]
  },
  {
    path: "reportes",
    loadChildren: () =>
      import("./Components/reportes/reportes.module").then(m => m.ReportesModule),
      canActivate:[AuthGuard]
  },
  {
    path: "catalogos",
    loadChildren: () =>
      import("./Components/catalogos/catalogos.module").then(m => m.CatalogosModule),
      canActivate:[AuthGuard]
  },
  {
    path: "acceso",
    loadChildren: () =>
      import("./Components/seguridad/seguridad.module").then(m => m.SeguridadModule),
      
  },
  {
    path: "",
    redirectTo:"/home",
    pathMatch: "full"
  },
  {
    path: "transacciones",
    loadChildren: () =>
      import("./Components/transacciones/transacciones.module").then(m => m.TransaccionesModule),
      canActivate:[AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
