export const environment = {
  production: true,
  baseUrl:'https://agroindustriaselsifon.azurewebsites.net/api',
  name: "Agroindustrias El Sifon"
};
